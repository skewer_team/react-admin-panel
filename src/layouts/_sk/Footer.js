import React from 'react';
import { Layout } from 'antd';
import LogPanel from '../../components/_sk/LogPanel/Index';

const { Footer } = Layout;

export default () => {
  return (
    <Footer style={{ padding: 0 }}>
      <LogPanel />
    </Footer>
  );
};
