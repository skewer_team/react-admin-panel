import React from 'react';
import {Spin} from "antd";
import DocumentTitle from 'react-document-title';
import isEqual from 'lodash/isEqual';
import memoizeOne from 'memoize-one';
import { connect } from 'dva';
import { ContainerQuery } from 'react-container-query';
import classNames from 'classnames';
import pathToRegexp from 'path-to-regexp';
import Media from 'react-media';
import withRouter from 'umi/withRouter';
import { formatMessage } from 'umi/locale';
import Initializer from '../../components/_sk/Initializer/Index';
import Context from '../MenuContext';
import { menu, title } from '../../defaultSettings';
import Loader from '../../components/_sk/Loader/Loader'
import SkCanapeLoader from '../../components/_sk/SkCanapeLoader/index'

import styles from './MainLayout.less';
import MainModule from '../../components/_sk/MainModule/Index'

const query = {
  'screen-xs': {
    maxWidth: 575,
  },
  'screen-sm': {
    minWidth: 576,
    maxWidth: 767,
  },
  'screen-md': {
    minWidth: 768,
    maxWidth: 991,
  },
  'screen-lg': {
    minWidth: 992,
    maxWidth: 1199,
  },
  'screen-xl': {
    minWidth: 1200,
    maxWidth: 1599,
  },
  'screen-xxl': {
    minWidth: 1600,
  },
};

@connect(({ loading }, { tabKey }) => {
  return {
    dropCacheEffect: !!loading.effects['skGlobal/dropCache'],
  };
})
class BasicLayout extends React.Component {
  constructor(props) {
    super(props);
    this.getPageTitle = memoizeOne(this.getPageTitle);
    this.matchParamsPath = memoizeOne(this.matchParamsPath, isEqual);
  }

  getContext() {
    const { location, breadcrumbNameMap } = this.props;
    return {
      location,
      breadcrumbNameMap,
    };
  }

  matchParamsPath = (pathname, breadcrumbNameMap) => {
    const pathKey = Object.keys(breadcrumbNameMap).find(key => pathToRegexp(key).test(pathname));
    return breadcrumbNameMap[pathKey];
  };


  getPageTitle = (pathname, breadcrumbNameMap) => {
    const currRouterData = this.matchParamsPath(pathname, breadcrumbNameMap);

    if (!currRouterData) {
      return title;
    }
    const pageName = menu.disableLocal
      ? currRouterData.name
      : formatMessage({
          id: currRouterData.locale || currRouterData.name,
          defaultMessage: currRouterData.name,
        });

    return `${pageName} - ${title}`;
  };


  render() {
    const {
      location: { pathname },
      breadcrumbNameMap,
      dropCacheEffect
    } = this.props;

    const layout = <MainModule {...this.props} />;

    return (
      <React.Fragment>
        <DocumentTitle title={this.getPageTitle(pathname, breadcrumbNameMap)}>
          <Spin spinning={dropCacheEffect} delay={0}>
            <ContainerQuery query={query}>
              {params => (
                <Context.Provider value={this.getContext()}>
                  <div className={classNames(params)}>{layout}</div>
                </Context.Provider>
              )}
            </ContainerQuery>
          </Spin>
        </DocumentTitle>
      </React.Fragment>
    );
  }
}

export default withRouter(connect(({ skGlobal, setting, menu: menuModel, loading }) => {
  return {
    storeInitialized: skGlobal.storeInitialized,
    breadcrumbNameMap: menuModel.breadcrumbNameMap,
    loading: loading.effects['skGlobal/fetchInitData'],
    ...setting,
  };
})(props => {
  return (
    <div>
      {props.loading ?  <SkCanapeLoader /> : ''}
      <Media query="(max-width: 599px)">
        {
          isMobile => {
            return props.storeInitialized
              ? <BasicLayout {...props} isMobile={isMobile} />
              : <Initializer />
          }
        }
      </Media>
    </div>
  );
}));
