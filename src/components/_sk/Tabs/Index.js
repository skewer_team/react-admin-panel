import React, { Component } from 'react';
import { Icon, Spin, Tabs } from 'antd';
import { connect } from 'dva';
import router from 'umi/router';
import TestForm from '../TestForm/Index';
import TestTable from '../TestTable/Index';
import Files from '../Files/Index';
import FileAddForm from '../FileAddForm/Index'
import SkCanapeLoader from '../SkCanapeLoader/index'

@connect(({ loading }) => {
  return {
    loading: !!loading.effects['skGlobal/loadTabs'] || !!loading.effects['skGlobal/postData4Tabs'],
  };
})
class Index extends Component {

  componentDidMount() {

    const {dispatch, tabsItemName} = this.props;

    if (tabsItemName){
      dispatch({
        type: 'skGlobal/fetchOneTab',
        payload: { activeKey: `out.tabs.${tabsItemName}` },
      });
    }

  }

  shouldComponentUpdate(nextProps, nextState, nextContext) {

    const {
      tabsItemName: tabsItemNameNext,
      leftPanelItemId: leftPanelItemIdNext,
      leftPanelItemName: leftPanelItemNameNext
    } = nextProps;

    // leftPanelItemIdNext равен '0'
    // когда мы переключаемся в новую вкладку сайдбрара
    if ( leftPanelItemIdNext === '0' ){
      return false;
    }


    return true;

  }

  componentDidUpdate(prevProps, prevState, snapshot) {

    const {dispatch, tabsItemName} = this.props;

    const {
      tabsItemName: tabsItemNamePrev
    } = prevProps;

    // Если изменились параметры роутинга, то меняет вкладку
    if ( tabsItemNamePrev !== tabsItemName ){
      dispatch({
        type: 'skGlobal/fetchOneTab',
        payload: { activeKey: `out.tabs.${tabsItemName}` },
      });
    }

  }

  buildContentOneTab = key => {
    const { tabs, loading } = this.props;

    if (!tabs.size) return '';

    const contentData = tabs.get(key);
    const extComponent = contentData.getIn(['params', 'extComponent']);

    let MainReactComponent = '';

    if ( contentData.size ){


      if ( (!extComponent || extComponent === 'Form') ) {

        if ( contentData.getIn(['moduleName']) === 'Files' ){

          switch ( contentData.getIn(['params', 'componentName']) ){
            case 'FileBrowserImages':
              MainReactComponent = <Files tabKey={key} moduleData={contentData} />;
              break;
            case 'FileAddForm':
              MainReactComponent = <FileAddForm tabKey={key} moduleData={contentData} />;
              break;
            default:

          }

        } else if (contentData.getIn(['params', 'items']))
          MainReactComponent = <TestForm tabKey={key} moduleData={contentData} />;
        else {
          MainReactComponent = '';
        }
      } else {
        MainReactComponent = <TestTable tabKey={key} moduleData={contentData} />;
      }

    }

    return (
      <Spin
        tip="Загрузка..."
        indicator={<SkCanapeLoader fadein main={false} loadText={false} />}
        spinning={loading}
      >
        {MainReactComponent}
      </Spin>
    );
  };

  handleChangeTab = activeKey => {
    const { leftPanelItemId, leftPanelItemName } = this.props;

    const newTab = activeKey.substring(activeKey.lastIndexOf('.') + 1);

    router.push({
      pathname: `/out.left.${leftPanelItemName}=${leftPanelItemId}/out.tabs=${newTab}`
    });
  };

  buildTabPanels = () => {
    const { tabs } = this.props;

    return tabs.valueSeq().map(val => {
      return (
        <Tabs.TabPane
          key={val.getIn(['path'])}
          tab={val.getIn(['params', 'componentTitle'])}
        >
          {this.buildContentOneTab(val.getIn(['path']))}
        </Tabs.TabPane>
      );
    });
  };

  render() {
    const { tabs, tabsItemName } = this.props;

    if (!tabs.size) {
      return '';
    }

    return (
      <Tabs
        className="sk-tabs"
        onChange={this.handleChangeTab}
        type="card"
        activeKey={`out.tabs.${tabsItemName}`}
      >
        {this.buildTabPanels()}
      </Tabs>
    );
  }
}

export default Index;
