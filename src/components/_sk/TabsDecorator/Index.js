import React, {Component} from 'react';
import {connect} from "dva";
import Tabs from '../Tabs/Index'
import * as treeAPI from '../../../dispatches/tree'
import * as sk from "../../../services/_sk/api";

@connect(({ skGlobal, loading }) => {
  return {
    tabs: skGlobal.tabs,
    hasDirtyForm: !!skGlobal.dirtyForms.length,
    loading: !!loading.effects['skGlobal/fetchOneTab'] || !!loading.effects['skGlobal/loadTabs'],
  };
})
class Index extends Component {

  componentDidMount() {

    const {leftPanelItemId, leftPanelItemName, dispatch} = this.props;

    if ( leftPanelItemId && (leftPanelItemId !== '0') && leftPanelItemName ){
      treeAPI.dispatchLoadTabs(dispatch, leftPanelItemId, leftPanelItemName)();
    }

  }

  componentDidUpdate(prevProps, prevState, snapshot) {

    const {
      leftPanelItemId,
      leftPanelItemName,
      dispatch,
      hasDirtyForm
    } = this.props;

    const {
      leftPanelItemId:leftPanelItemIdPrev,
      leftPanelItemName:leftPanelItemNamePrev
    } = prevProps;

    const routerParamsChanged = ( (leftPanelItemId !== leftPanelItemIdPrev) || (leftPanelItemName !== leftPanelItemNamePrev) );

    if ( routerParamsChanged && leftPanelItemId && (leftPanelItemId !== '0') ){

      if ( hasDirtyForm ){
        sk.showModal(
          sk.dict('editorCloseConfirmHeader'),
          sk.dict('editorCloseConfirm'),
          treeAPI.dispatchLoadTabs(dispatch, leftPanelItemId, leftPanelItemName)
        );
      } else {
        treeAPI.dispatchLoadTabs(dispatch, leftPanelItemId, leftPanelItemName)();
      }

    }

  }

  render() {

    const {tabs, tabsItemName, leftPanelItemId, leftPanelItemName} = this.props;

    return (
      <Tabs
        tabs={tabs}
        tabsItemName={tabsItemName}
        leftPanelItemId={leftPanelItemId}
        leftPanelItemName={leftPanelItemName}
      />
    );
  }
}

export default Index;
