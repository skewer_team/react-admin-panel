import React from 'react';
import { Collapse } from 'antd';
import style from './style.less';

function getPanels(groups){
  const panels = [];

  Object.entries(groups).forEach(([key,item]) => {
    
    panels.push(
      <Collapse.Panel
        key={key}
        showArrow={!!item.collapsible}
        header={item.groupTitle}
      >
        {item.items}
      </Collapse.Panel>
    );

  });

  return panels;
}

function getActiveKeys(groups){
  const activeKeys = [];
  Object.entries(groups).forEach(([key, value]) => {
    if (!value.collapsed) {
      activeKeys.push(key);
    }
  });

  return activeKeys;
}

export default (props) => {

  const { groups } = props;

  return (
    <Collapse
      defaultActiveKey={getActiveKeys(groups)}
      className="sk-content-collapse"
    >
      {getPanels(groups)}
    </Collapse>
  );

}
