import React, { useState, useEffect } from 'react';
import { connect } from 'dva';
import { Collapse } from 'antd';
import { groupBy } from 'lodash';
import router from "umi/router";
import Scrollbars from 'react-custom-scrollbars';
import { vh, addEvent } from '../helpers/Index';
import SkList from '../skList/Index';
import styles from './style.less';

const handleClickListItem = itemId => () => {

  router.push({
    pathname: `out.left.tools=${itemId}`
  });

};

const renderPanels = (moduleData) => {
  const { Panel } = Collapse;

  const leftPanelItemId = moduleData.get('lastActiveItemId') || 0;

  const dataSource = moduleData.getIn(['params', 'items']).toJS();

  const groups = groupBy(dataSource, 'group');

  return Object.entries(groups).map(([index, value]) => {
    return (

      <Panel
        key={index}
        className="skleftManagment"
        header={index}
      >

        <SkList
          items={value}
          selectedItemId={leftPanelItemId}
          handleClick={handleClickListItem}
        />
      </Panel>
    );
  });
};

export default connect(({ skGlobal }) => {
  return {
    moduleData: skGlobal.leftLayout.get('out.left.tools'),
  };
})(({ moduleData }) => {

  const [maxHeight, setMaxHeight] = useState(vh()) 

  useEffect(() => {
    addEvent(window, "resize", (event)  => {
      setMaxHeight(vh())
    });
  })

  const dataSource = moduleData.getIn(['params', 'items']).toJS();

  const groups = groupBy(dataSource, 'group');

  return (
    <Scrollbars
      // This will activate auto-height
      className="sktree-scrollbar"
      autoHide
      autoHideTimeout={1000}
      autoHideDuration={200}
      autoHeight
      autoHeightMin={100}
      autoHeightMax={maxHeight - 380}
    >
      <Collapse
        bordered={false}
        className="sk-collapse-tools"
        defaultActiveKey={Object.keys(groups)}
      >
        {renderPanels(moduleData)}
      </Collapse>
    </Scrollbars>
  );
})
