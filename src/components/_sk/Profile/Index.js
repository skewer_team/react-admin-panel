import React from 'react';
import {Button, Divider, Dropdown, Icon} from "antd";
import stylesMain from "../../../layouts/_sk/MainLayout.less";
import styles from "./style.less";
import UilUser from '@iconscout/react-unicons/icons/uil-user';


const handleClickOnLogout = (props) => () => {

  const {dispatch} = props;

  dispatch({
    type: 'skGlobal/logout',
    payload: {
      path: 'out.header.auth',
    }
  });

};

export default (props) => {

  const {moduleData, className} = props;

  if (!moduleData)
    return null;

  const langValues = moduleData.getIn(['init', 'lang']);

  const renderData = moduleData.getIn(['init', 'renderData']);

  return (
    <Dropdown
      className={className}
      overlay={
        <div className="nav-top_user-dd">
          <div>
            <p>{renderData.get('username')}</p>
            <p>{langValues.get('authLastVisit')}: {renderData.get('lastlogin')}</p>
          </div>
          <Divider />
          <Button
            className="sk-main-button"
            type='primary'
            onClick={handleClickOnLogout(props)}
          >
            {langValues.get('authLogoutButton')}
          </Button>
        </div>
      }
    >
      <span className={`${stylesMain.action}`}>
        {/* <Icon type="user" /> */}
        <UilUser type="user" size="20" className="unicons unicons-globe" />
      </span>
    </Dropdown>
  );

}
