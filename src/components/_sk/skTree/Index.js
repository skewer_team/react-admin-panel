import React from 'react';
import {Button, Col, Icon, Row, Spin, Tree} from 'antd';
import { connect } from 'dva';
import { groupBy } from 'lodash';
import classNames from 'classnames';
import router from "umi/router";
import * as Unicons from '@iconscout/react-unicons';
import Scrollbars from 'react-custom-scrollbars';
import EditSectionForm from '../EditSectionForm/Index';
import * as treeApi from '../../../dispatches/tree';
import style from './style.less';
import mainStyles from '../../../layouts/_sk/MainLayout.less';
import { vh, addEvent } from '../helpers/Index';
import * as sk from "../../../services/_sk/api";
import SkCanapeLoader from '../SkCanapeLoader/index'

const { TreeNode } = Tree;

@connect(({ skGlobal, loading }) => {
  return {
    sidebarActiveItem: skGlobal.sidebarActiveItem,
    activeTab: skGlobal.activeTab,
    loading:
      !!loading.effects['skGlobal/getSubItems'] ||
      !!loading.effects['skGlobal/deleteSection'] ||
      !!loading.effects['skGlobal/saveSection'],
  };
})
class Index extends React.Component {

  state = {
    visibleModalWindowEditNode: false,
    maxHeight: vh()
  };

  componentDidMount() {

    const {leftPanelItemId} = this.props;

    if (leftPanelItemId){
      this.selectSection(leftPanelItemId);
    }
  
    addEvent(window, "resize", (event)  => {
      this.setState({
        maxHeight: vh()
      });
    });

  }

  componentDidUpdate(prevProps, prevState, snapshot) {

    const {
      leftPanelItemId: activeItemIdCur,
      sidebarActiveItem,
      moduleName
    } = this.props;

    const {
      leftPanelItemId: activeItemIdPrev,
      sidebarActiveItemPrev
    } = prevProps;

    // Дерево находится в активной вкладке родителя
    const locatedInsideActivePanel = moduleName === sidebarActiveItem;

    // Если параметры роутинга изменились и дерево находится в активной вкладке
    if ( ((activeItemIdCur !== activeItemIdPrev) || (sidebarActiveItemPrev !== sidebarActiveItem)) && locatedInsideActivePanel ){
      this.selectSection(activeItemIdCur);
    }

  }

  selectSection = (itemId) => {

    const {dispatch, moduleData} = this.props;

    const sections = moduleData.getIn(['params', 'items']).toJS();

    // ид добавленного раздела
    const addedSectionId = moduleData.get('addedSectionId', 0);

    // Если раздела, который должен быть выбран, нет в сторе
    if ( parseInt(itemId) ){

      if ( addedSectionId === parseInt(itemId) ){

        dispatch({
          type: 'skGlobal/clearAddedSectionId',
          payload: {
            path: moduleData.get('path')
          }
        });

        dispatch({
          type: 'skGlobal/getTree',
          payload: {
            path: moduleData.get('path'),
            sectionId: itemId
          }
        });

      } else if ( !Object.keys(sections).includes(itemId.toString()) ){

        dispatch({
          type: 'skGlobal/getTree',
          payload: {
            path: moduleData.get('path'),
            sectionId: itemId
          }
        });

      }


    }

  };

  onSelect = (selectedKeys, info) => {
    const { dispatch, moduleData, openNodes, sidebarActiveItem, activeTab } = this.props;

    const { node } = info;

    if (selectedKeys.length) {

      router.push({
        pathname: `/out.left.${sidebarActiveItem}=${selectedKeys[0]}/out.tabs=${activeTab}`
      });

    }

    if (!node.isLeaf()) {
      if (!node.getNodeChildren().length) {
        // Загружаем поддерево
        treeApi.dispatchGetSubItems(dispatch, node.props.eventKey, moduleData.get('path'));
      }

      if (!openNodes.includes(node.props.eventKey)) {
        // Меняем state, добавляя данный узел в список "раскрытых"
        dispatch({
          type: 'skGlobal/extendedTree',
          payload: {
            expandedKeys: openNodes.concat([node.props.eventKey]),
            path: moduleData.get('path')
          }
        });
      }
    }
  };

  handleDeleteSection = (section) => ev => {
    // останавливаем всплытие события,
    // для того чтобы событие onClick не сработало на родитеских элементах
    ev.stopPropagation();

    const { dispatch, moduleData } = this.props;

    const headerModalWindow = (
      <span className="sk-modal-delete-header">
        {moduleData.getIn(['init', 'lang', 'treeDelRowHeader'])}
      </span>
    );
    const textModalWindow = (
      <div className="sk-modal-delete">
        {/* Удалить "имя_раздела"? */}
        <div>{sk.dict('delRow').replace('{0}', section.title)}</div>
        <div>{`${moduleData.getIn(['init', 'lang', 'treeDelMsg'])}`}</div>
      </div>
    );

    sk.showModal(headerModalWindow, textModalWindow, () => {
      dispatch({
        type: 'skGlobal/deleteSection',
        payload: {
          path: moduleData.get('path'),
          sectionId: section.id,
        },
      });
    });

  };

  hideWindow = () => {
    this.setState({
      visibleModalWindowEditNode: false,
    });
  };
  
  renderModalWindowEditTreeNode = () => {
    const { visibleModalWindowEditNode } = this.state;

    const {moduleData} = this.props;

    return <EditSectionForm
      parentModulePath={moduleData.get('path')}
      visible={visibleModalWindowEditNode}
      hideWindow={this.hideWindow}
    />;
  };

  handleEditTreeNode = item => () => {
    const { dispatch, moduleData } = this.props;

    treeApi.dispatchGetForm(dispatch, item, moduleData.get('path'));

    // Показать модальное окно
    this.setState({
      visibleModalWindowEditNode: true,
    });
  };



  renderBranch = (items, parentPath = []) => {
    return Object.entries(items).map(([index, value]) => {
      const children =
        value.children && value.children.length
          ? this.renderBranch(value.children, parentPath.concat(index, 'children'))
          : null;
      const isLeaf = value.children !== undefined && !value.children.length;



      const title = (
        <span className={classNames("tree-item", {"tree-item--hidden" : ![1,2,-1].includes(value.visible) })}>
          <span title={value.title} className="tree-item__title">{value.title}</span>
          <span className="tree-item__elements">
            <span className="tree-item__id">{value.id}</span>
            <i
              className="unicons tree-item__edit"
              type="uil-edit"
              title="редактировать"
              onClick={this.handleEditTreeNode(value)}
            >
              <Unicons.UilEditAlt size="14" />
            </i>

            <i
              className="unicons tree-item__delete"
              type="uil-trash"
              title="удалить"
              onClick={this.handleDeleteSection(value)}
            >
              <Unicons.UilTrash size="14" />
            </i>
          </span>
        </span>
      );

      return (
        <TreeNode
          icon={({skData}) => {

            // Тип "директория" ?
            if ( skData.type === 1 ){
              return <Icon type="folder" />
            }

            if ( !skData.type ){
              return skData.link
                ? <Unicons.UilLinkAlt className="unicons" size="14" />
                : <Unicons.UilFileBlank className="unicons" size="14" />
            }

          }}
          switcherIcon={(props) => {
            const { isLeaf , expanded} = props;
            // Раскрыт?
            if ( expanded ){
              return <Unicons.UilMinus className="unicons" size="14" />
            }

            // Свёрнут?
            if ( !expanded ){

              // Лист?
              if ( isLeaf ){
                return <span></span>;
              }
              return <Unicons.UilPlus className="unicons" size="14" />

            }

            return null;

          }}
          title={title}
          key={value.id}
          nodeId={value.id}
          skData={value}
          isLeaf={isLeaf}
          children={children}
        />
      );
    });
  };

  onExpand = (expandedKeys, { expanded, node }) => {
    const { dispatch, moduleData } = this.props;

    const { children, nodeId } = node.props;

    if (expanded && (!children || !children.length)) {
      // Запрос поддерева
      treeApi.dispatchGetSubItems(dispatch, nodeId, moduleData.get('path'));
    }

    // Обновляем стор
    dispatch({
      type: 'skGlobal/extendedTree',
      payload: {
        expandedKeys,
        path: moduleData.get('path')
      }
    });
  };

  // todo Перенести в сервисы
  collect = (sectionId, listAllSection) => {
    if (!listAllSection[sectionId]) return [];

    const out = [];

    listAllSection[sectionId].forEach(val => {
      if (!listAllSection[val.id]) {
        // val.children = [];
        out.push(val);
      } else {
        val.children = this.collect(val.id, listAllSection);
        out.push(val);
      }
    });

    out.sort((a, b) => {

      if ( a.position === b.position ){
        return 0;
      }

      return a.position > b.position ? 1 : -1
    });

    return out;
  };

  handleClickAddSection = () => {

    const { dispatch, moduleData, typeNewPage, leftPanelItemId } = this.props;

    const rootSectionId = moduleData.getIn(['init', 'rootSection']);

    const item = {
      id: 0,
      title: moduleData.getIn(['init','lang', 'treeNewSection']),
      parent: parseInt(leftPanelItemId || rootSectionId),
      type: typeNewPage,
      visible: 1,
    };

    treeApi.dispatchGetForm(dispatch, item, moduleData.get('path'));

    // Показать модальное окно
    this.setState({
      visibleModalWindowEditNode: true,
    });

  };

  renderPanelBlock = () => {

    const {addButtons, moduleData} = this.props;

    return (
      <Row gutter={8}>
        <Col span={12}>
          <Button
            type="primary"
            className="sk-main-button"
            htmlType="button"
            block
            size="small"
            onClick={this.handleClickAddSection}
          >
            {moduleData.getIn(['init','lang','add'])}
          </Button>
        </Col>
        {addButtons}
      </Row>
    )

  };

  onDrop = info => {
    const dropKey = info.node.props.eventKey;
    const dragKey = info.dragNode.props.eventKey;
    const dropPos = info.node.props.pos.split('-');
    const dropPosition = info.dropPosition - Number(dropPos[dropPos.length - 1]);

    const {dispatch, moduleData} = this.props;

    dispatch({
      type: 'skGlobal/changePosition',
      payload: {
        path: moduleData.get('path'),
        dropKey,
        dragKey,
        dropPos,
        dropPosition
      }
    })
  };

  render() {
    const { moduleData, openNodes, loading, leftPanelItemId } = this.props;

    const { maxHeight } = this.state;

    // todo
    if (!moduleData) {
      return '';
    }

    // console.log('Список items', moduleData.getIn(['params', 'items']).toJS());

    const groupByParent = groupBy(moduleData.getIn(['params', 'items']).toJS(), val => {
      return val.parent;
    });

    const baseNodeId = moduleData.getIn(['init', 'rootSection']);

    // console.log('Сгруппированные по parent', groupByParent);

    // записи в виде дерева
    const itemsInTreeView = this.collect(baseNodeId, groupByParent);

    // console.log('Дерево', itemsInTreeView);

    const treeNodes = this.renderBranch(itemsInTreeView);

    return (
      <React.Fragment>
        <Spin
          tip="Загрузка..."
          // indicator={<Icon type="loading" style={{ fontSize: 32 }} spin />}
          indicator={<SkCanapeLoader width={40} fadein main={false} loadText={false} />}
          spinning={loading}
        >
          {this.renderPanelBlock()}
          <Scrollbars
            // This will activate auto-height
            className="sktree-scrollbar"
            autoHide
            autoHideTimeout={1000}
            autoHideDuration={200}
            autoHeight
            autoHeightMin={100}
            autoHeightMax={maxHeight - 380}
          >
            <Tree
              className="hide-file-icon"
              showIcon
              blockNode
              showLine
              onSelect={this.onSelect}
              expandedKeys={openNodes}
              selectedKeys={[leftPanelItemId]}
              onExpand={this.onExpand}
              draggable
              onDragEnter={this.onDragEnter}
              onDrop={this.onDrop}
            >
              {treeNodes}

            </Tree>
          </Scrollbars>
        </Spin>
        {this.renderModalWindowEditTreeNode()}
      </React.Fragment>
    );
  }
}

export default Index;
