import React from 'react';
import { Form, TimePicker } from 'antd';
import moment from 'moment';
import FieldPrototype from '../FieldPrototype';
import styles from './style.less';

export default class Index extends FieldPrototype {
  getDecorator() {
    const format = 'HH:mm';
    const { getFieldDecorator } = this.form;

    return getFieldDecorator(this.configField.get('name'), {
      skFieldInstance: this,
      initialValue: moment(this.configField.get('value'), format),
      rules: [{ required: false, message: '' }],
    })(
      <TimePicker
        disabled={!!this.configField.get('disabled')}
        format={format}
      />
    );
  }

  getFormFieldItem() {
    const decorator = this.getDecorator();

    return (
      <Form.Item
        key={this.configField.get('name')}
        help={<div dangerouslySetInnerHTML={{ __html: this.configField.get('subtext') }} />}
        label={this.configField.get('title')}
      >
        {decorator}
      </Form.Item>
    );
  }

  getHandledValue = () => {
    // in format moment.js
    const valueInMomentFormat = this.form.getFieldValue(this.name);
    if (valueInMomentFormat) {
      return valueInMomentFormat.format('HH:mm:ss');
    }
  };

}
