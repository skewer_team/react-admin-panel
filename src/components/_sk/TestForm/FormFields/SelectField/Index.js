import React from 'react';
import { Form, Select, Icon } from 'antd';
import Immutable from 'immutable';
import FieldPrototype from '../FieldPrototype';
import styles from './style.less';

export default class Index extends FieldPrototype {
  
  getDecorator() {
    const { getFieldDecorator } = this.form;

    const valueField = this.configField.get('valueField');
    const displayField = this.configField.get('displayField');

    const options = this.configField
      .getIn(['store', 'data'])
      .valueSeq()
      .map(dataVal => {
        const disabledVariants = this.configField.get('disabledVariants');

        let disabled = false;
        let position = -1;

        if (disabledVariants) {
          position = disabledVariants.findIndex(
            disabledValue => disabledValue === dataVal.get(valueField)
          );
          disabled = position !== -1;
        }

        return (
          <Select.Option
            disabled={disabled}
            key={dataVal.get(displayField).toString()}
            value={dataVal.get(valueField).toString()}
          >
            <div dangerouslySetInnerHTML={{ __html: dataVal.get(displayField) }} />
          </Select.Option>
        );
      });

    let initialValue = Immutable.List.isList(this.configField.get('value'))
      ? this.configField.get('value').toJS()
      : this.configField.get('value');

    initialValue = !_.isArray(initialValue)
      ? _.compact(initialValue.toString().split(','))
      : initialValue;

    // Приведение элементов массива к строке
    initialValue = initialValue.map(val => val.toString());

    return getFieldDecorator(this.configField.get('name'), {
      initialValue,
      skFieldInstance: this,
      rules: [{ required: false, message: '' }],
    })(
      <Select
        mode={this.configField.get('type') === 'multiselect' ? 'multiple' : 'default'}
        disabled={!!this.configField.get('disabled')}
        suffixIcon={<span />}

      >
        {options.toJS()}
      </Select>
    );
  }

  getHandledValue = () => {
    const nameField = this.configField.get('name');
    const value = this.form.getFieldValue(nameField);
    return _.isArray(value) ? value.join(',') : value
  };

  getFormFieldItem() {
    const decorator = this.getDecorator();

    return (
      <Form.Item
        key={this.configField.get('name')}
        help={<div dangerouslySetInnerHTML={{ __html: this.configField.get('subtext') }} />}
        label={this.configField.get('title')}
      >
        {decorator}
      </Form.Item>
    );
  }
}
