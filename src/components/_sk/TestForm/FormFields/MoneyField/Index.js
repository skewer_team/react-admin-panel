import React from 'react';
import { Form, InputNumber } from 'antd';
import FieldPrototype from '../FieldPrototype';

export default class Index extends FieldPrototype {
  getDecorator() {
    const { getFieldDecorator } = this.form;

    const config = {
      precision: 2 // округлять до двух знаков
    };

    if (this.configField.get('disabled') !== undefined) {
      config.disabled = !!this.configField.get('disabled');
    }

    if (this.configField.get('minValue') !== undefined) {
      config.min = this.configField.get('minValue');
    }

    if (this.configField.get('maxValue') !== undefined) {
      config.max = this.configField.get('maxValue');
    }

    if (this.configField.get('step') !== undefined) {
      config.step = this.configField.get('step');
    }

    return getFieldDecorator(this.configField.get('name'), {
      skFieldInstance: this,
      initialValue: this.configField.get('value'),
      rules: [{ required: false, message: '' }],
    })(
      <InputNumber {...config} />
    );
  }

  getFormFieldItem() {
    const decorator = this.getDecorator();

    return (
      <Form.Item
        key={this.configField.get('name')}
        help={<div dangerouslySetInnerHTML={{ __html: this.configField.get('subtext') }} />}
        label={this.configField.get('title')}
      >
        {decorator}
      </Form.Item>
    );
  }
}
