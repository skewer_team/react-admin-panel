import React from 'react';
import { Form, Input } from 'antd';
import FieldPrototype from '../FieldPrototype';

export default class Index extends FieldPrototype {
  getDecorator() {
    const { getFieldDecorator } = this.form;

    return getFieldDecorator(this.configField.get('name'), {
      skFieldInstance: this,
      initialValue: this.configField.get('value'),
      rules: [{ required: false, message: '' }],
    })(<Input.TextArea disabled={!!this.configField.get('disabled')} rows={4} />);
  }

  getFormFieldItem() {
    const decorator = this.getDecorator();
    
    return (
      <Form.Item
        {...{
          labelCol: { span: 23 },
          wrapperCol: { span: 24 },
        }}
        key={this.configField.get('name')}
        help={<div dangerouslySetInnerHTML={{ __html: this.configField.get('subtext') }} />}
        label={this.configField.get('title')}
      >
        {decorator}
      </Form.Item>
    );
  }
}
