import React from 'react';
import { Form } from 'antd';
import CKEditor from 'ckeditor4-react';
import FieldPrototype from '../FieldPrototype';

export default class Index extends FieldPrototype {
  getDecorator() {
    const { getFieldDecorator } = this.form;

    const addConfig = this.configField.getIn(['addConfig']).toJS();

    const { NODE_ENV } = process.env;

    window.skWyswygConfig = addConfig;

    return getFieldDecorator(this.configField.get('name'), {
      skFieldInstance: this,
      initialValue: this.configField.get('value'),
      rules: [{ required: false, message: '' }],
      getValueFromEvent: (e) => {

        if (e.name === 'change'){
          return e.editor.getData();
        }

        return e;

      },
      getValueProps: (value) => {
        return {
          data: value
        }
      }
    })(
      <CKEditor
        data={this.configField.get('value')}
        config={{
          customConfig: NODE_ENV === 'development' ? `/config_new.js` : `${ckedir}/config_new.js`,
          ...addConfig
        }}
      />
    );
  }

  getFormFieldItem() {
    const decorator = this.getDecorator();

    return (
      <Form.Item
        {...{
          labelCol: { span: 23 },
          wrapperCol: { span: 24 },
        }}
        key={this.configField.get('name')}
        help={<div dangerouslySetInnerHTML={{ __html: this.configField.get('subtext') }} />}
        label={this.configField.get('title')}
      >
        {decorator}
      </Form.Item>
    );
  }


  /**
   * Получить "обработанное" (подготовленное для отправки на сервер) значение
   * */
  getHandledValue = () => {
    const value = this.form.getFieldValue(this.name);

    if ( typeof value === 'string' ){
      return value;
    }

    if (value && value.editor) {
      return value.editor.getData();
    }
    return '';

  };


}
