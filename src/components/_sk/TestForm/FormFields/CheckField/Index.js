import React from 'react';
import { Form, Checkbox } from 'antd';
import FieldPrototype from '../FieldPrototype';
import styles from './style.less';


export default class Index extends FieldPrototype {

  getHandledValue = () => {
    return this.form.getFieldValue(this.name) ? 1 : 0;
  };

  getDecorator() {
    const { getFieldDecorator } = this.form;

    return getFieldDecorator(this.configField.get('name'), {
      skFieldInstance: this,
      initialValue: !!(this.configField.get('value') !== '0' && this.configField.get('value')),
      // normalize(v){
      //     return ;
      // },
      // getValueProps(val1){
      //     console.log(val.get('name'), !!(val1 !== "0" && val1));
      //     return {
      //         "data-__field": {
      //             name: val.get('name'),
      //             value: !!(val1 !== "0" && val1)
      //         },
      //         checked: !!(val1 !== "0" && val1),
      //         value: !!(val1 !== "0" && val1)
      //     };
      // },
      valuePropName: 'checked',
      rules: [{ required: false, message: '' }],
    })(<Checkbox checked disabled={!!this.configField.get('disabled')}>{this.configField.get('title')}</Checkbox>);
  }

  getFormFieldItem() {
    const decorator = this.getDecorator();

    return (
      <React.Fragment>
        <div className="sk-checkbox_wrap">
          <div className="sk-checkbox">{decorator}</div>
          <div className="sk-checkbox_label">
            <Form.Item
              key={this.configField.get('name')}
              help={<div dangerouslySetInnerHTML={{ __html: this.configField.get('subtext') }} />}
              label={this.configField.get('title')}
            >
            </Form.Item>
          </div>
        </div>
        
      </React.Fragment>
    );
  }
}
