import React from 'react';
import { Form, DatePicker } from 'antd';
import moment from 'moment';
import FieldPrototype from '../FieldPrototype';
import style from './style.less'

export default class Index extends FieldPrototype {
  getDecorator() {
    const { getFieldDecorator } = this.form;

    return getFieldDecorator(this.configField.get('name'), {
      skFieldInstance: this,
      initialValue: moment(this.configField.get('value')),
      rules: [{ required: false, message: '' }],
    })(
      <DatePicker
        format="DD.MM.YYYY HH:mm"
        disabled={!!this.configField.get('disabled')}
        showTime
      />
    );
  }

  getFormFieldItem() {
    const decorator = this.getDecorator();

    return (
      <Form.Item
        key={this.configField.get('name')}
        help={<div dangerouslySetInnerHTML={{ __html: this.configField.get('subtext') }} />}
        label={this.configField.get('title')}
      >
        {decorator}
      </Form.Item>
    );
  }

  getHandledValue = () => {
    // in format moment.js
    const valueInMomentFormat = this.form.getFieldValue(this.name);
    return valueInMomentFormat.format('YYYY-MM-DD HH:mm:ss');
  };

}
