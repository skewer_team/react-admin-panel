export default class FieldPrototype {

  name = '';

  /** Данные переданные от родитеского модуля */
  serviceModuleData = [];

  constructor(moduleData, { configField, form }) {
    this.configField = configField;
    this.form = form;
    this.name = this.configField.get('name');
    this.moduleData = moduleData;
    this.serviceModuleData = moduleData.getIn(['params', 'serviceData']).toJS();
  }

  /**
   * Получить "обработанное" (подготовленное для отправки на сервер) значение
   * */
  getHandledValue = () => {
    return this.form.getFieldValue(this.name);
  };

}
