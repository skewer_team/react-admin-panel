import React from 'react';
import { Form, InputNumber } from 'antd';
import FieldPrototype from '../FieldPrototype';
import styles from './style.less';

export default class Index extends FieldPrototype {
  getDecorator() {
    const { getFieldDecorator } = this.form;

    const config = {};

    if (this.configField.get('disabled') !== undefined) {
      config.disabled = !!this.configField.get('disabled');
    }

    if (this.configField.get('minValue') !== undefined) {
      config.min = this.configField.get('minValue');
    }

    if (this.configField.get('maxValue') !== undefined) {
      config.max = this.configField.get('maxValue');
    }

    // if ( this.configField.get('allowDecimals') !== undefined ){
    //     config.step = 0.1;
    // }

    return getFieldDecorator(this.configField.get('name'), {
      skFieldInstance: this,
      initialValue: this.configField.get('value'),
      rules: [{ required: false, message: 'asdasd' }],
    })(<InputNumber {...config} />);
  }

  getFormFieldItem() {
    const decorator = this.getDecorator();

    return (
      <Form.Item
        key={this.configField.get('name')}
        help={<div dangerouslySetInnerHTML={{ __html: this.configField.get('subtext') }} />}
        label={this.configField.get('title')}
      >
        {decorator}
      </Form.Item>
    );
  }
}
