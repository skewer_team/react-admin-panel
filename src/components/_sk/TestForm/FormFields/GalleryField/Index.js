import React from 'react';
import {Button, Form, Icon, Input, message, Upload} from 'antd';
import FieldPrototype from '../FieldPrototype';
import * as sk from "../../../../../services/_sk/api";

export default class Index extends FieldPrototype {

  parseUrl = name => {
    const escapedName = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    const regexS = `[\\?&\\.]${escapedName}=([^&#\/]*)`;
    const regex = new RegExp( regexS );
    const results = regex.exec( window.location.href );
    if (results == null) {
      return '';
    }
    return results[1];
  };

  handleButtonGallery = () => {

    let makeNewAlbum = 0;

    const selectMode = this.configField.get('mode', 'galleryBrowser');
    let selectValue = parseInt(this.configField.get('value', '0'));
    if ( !selectValue ) {
      selectValue = 0;
      makeNewAlbum = 1;
    }
    const GalProfileId = this.configField.get('gal_profile_id', '0');
    const seoClass = this.configField.get('seoClass', '');
    const iEntityId = this.configField.get('iEntityId', 0);
    const sectionId = this.configField.get('sectionId', 0);

    const fieldName = this.configField.get('name');
    const path = this.moduleData.get('path');

    // собрать ссылку
    const href = `/oldadmin/?mode=${selectMode}&cmd=showAlbum&gal_album_id=${selectValue}&gal_profile_id=${GalProfileId}&gal_new_album=${makeNewAlbum}&seoClass=${seoClass}&iEntityId=${iEntityId}&sectionId=${sectionId}&fieldName=${fieldName}&path=${path}`;

    // открыть в новом окне
    sk.newWindow( href );

    return true;

  };

  handleButtonRecreate = () => {

    const makeNewAlbum = 1;

    const selectMode = this.configField.get('mode', 'galleryBrowser');
    let selectValue = parseInt(this.configField.get('value', '0'));
    if ( !selectValue ) {
      selectValue = 0;
    }
    const GalProfileId = this.configField.get('gal_profile_id', '0');
    const seoClass = this.configField.get('seoClass', '');
    const iEntityId = this.configField.get('iEntityId', 0);
    const sectionId = this.configField.get('sectionId', 0);

    const fieldName = this.configField.get('name');
    const path = this.moduleData.get('path');

    // собрать ссылку
    const href = `/oldadmin/?mode=${selectMode}&cmd=showAlbum&gal_album_id=${selectValue}&gal_profile_id=${GalProfileId}&gal_new_album=${makeNewAlbum}&seoClass=${seoClass}&iEntityId=${iEntityId}&sectionId=${sectionId}&fieldName=${fieldName}&path=${path}`;

    if (selectValue) {
      sk.showModal(sk.dict('confirmHeader'), sk.dict('galleryBrowserNewConfirm'), () => {
        sk.newWindow( href );
      });
    } else {
      sk.newWindow( href );
    }

    return true;

  };

  getDecorator() {
    const { getFieldDecorator } = this.form;

    return getFieldDecorator(this.configField.get('name'), {
      skFieldInstance: this,
      initialValue: this.configField.get('value'),
      rules: [{ required: false, message: '' }],
    })(
      <Input disabled />
    );
  }

  getFormFieldItem() {
    const decorator = this.getDecorator();

    return (
      <Form.Item
        key={this.configField.get('name')}
        help={<div dangerouslySetInnerHTML={{__html: this.configField.get('subtext')}} />}
        label={this.configField.get('title')}
      >
        {decorator}
        <Button onClick={this.handleButtonGallery}>
          {sk.dict('fileBrowserSelect')}
        </Button>
        <Button onClick={this.handleButtonRecreate}>
          {sk.dict('galleryBrowserNew')}
        </Button>
      </Form.Item>
    );
  }

}
