import StringField from './StringField';
import HideField from './HideField';
import PassField from './PassField/Index';
import TextField from './TextField/Index';
import SelectField from './SelectField/Index';
import CheckField from './CheckField/Index';
import TimeField from './TimeField/Index';
import DateField from './DateField/Index';
import DateTimeField from './DateTimeField/Index';
import NumField from './NumField/Index';
import FloatField from './FloatField/Index';
import MoneyField from './MoneyField/Index';
import ShowField from './ShowField/Index';
import Wyswyg from './WyswygField/Index'
import File from './File/Index'
import SlideShower from './SlideShower/Index'
import GalleryField from './GalleryField/Index'

class FieldsFactory {
  static createFormField(type, moduleData, options) {
    let instance = null;

    const {configField} = options;

    switch (type) {
      case 'str':
        instance = new StringField(moduleData, options);
        break;
      case 'hide':
        instance = new HideField(moduleData, options);
        break;
      case 'pass':
        instance = new PassField(moduleData, options);
        break;

      case 'wyswyg':

        instance = new Wyswyg(moduleData, options);
        break;

      case 'text':
        instance = new TextField(moduleData, options);
        break;

      case 'select':
      case 'multiselect':

        //todo Сделать редактируемый селект
        if ( configField.get('editable') ){
          instance = new StringField(moduleData, options);
        } else {
          instance = new SelectField(moduleData, options);
        }

        break;

      case 'check':
        instance = new CheckField(moduleData, options);
        break;

      case 'time':
        instance = new TimeField(moduleData, options);
        break;

      case 'date':
        instance = new DateField(moduleData, options);
        break;

      case 'datetime':
        instance = new DateTimeField(moduleData, options);
        break;

      case 'num':
        instance = new NumField(moduleData, options);
        break;

      case 'float':
        instance = new FloatField(moduleData, options);
        break;

      case 'money':
        instance = new MoneyField(moduleData, options);
        break;

      case 'show':
        instance = new ShowField(moduleData, options);
        break;

      case 'file':
        instance = new File(moduleData, options);
        break;

      case 'specific':

        switch (options.configField.get('extendLibName')) {
          case "SlideShower":
            instance = new SlideShower(moduleData, options);
            break;
          default:
          // throw new Exception('Unknown specific field`s class');
        }

        break;

      case 'gallery':
        instance = new GalleryField(moduleData, options);
        break;

      case 'text_html':
      case 'text_js':
      case 'text_css':
      case 'imagefile':
      case 'html':
      case 'inherit':
      case 'colorselector':
      case 'mapSingleMarker':
      case 'mapListMarkers':
      case 'paymentObject':
      case 'selectimage':
      default:
      // throw new Exception('Unknown field`s class');
    }

    return instance ? instance.getFormFieldItem() : null;
  }
}

export default FieldsFactory;
