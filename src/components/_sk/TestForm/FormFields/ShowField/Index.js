import React from 'react';
import { Form } from 'antd';
import FieldPrototype from '../FieldPrototype';

export default class Index extends FieldPrototype {
  getDecorator() {
    const { getFieldDecorator } = this.form;
    const value = this.configField.get('value');

    return getFieldDecorator(this.configField.get('name'), {
      skFieldInstance: this,
      rules: [{ required: false, message: '' }],
    })(<div dangerouslySetInnerHTML={{ __html: value }} />);
  }

  getFormFieldItem() {
    const decorator = this.getDecorator();

    return (
      <Form.Item
        key={this.configField.get('name')}
        className="sk-form-link"
        help={<div dangerouslySetInnerHTML={{ __html: this.configField.get('subtext') }} />}
        label={this.configField.get('title')}
      >
        {decorator}
      </Form.Item>
    );
  }
}
