import React from 'react';
import {Button, Form, Icon, Input, message, Upload} from 'antd';
import FieldPrototype from '../FieldPrototype';
import * as sk from "../../../../../services/_sk/api";

export default class Index extends FieldPrototype {

  handleOnChange = () => (info) => {

    if (info.file.status === 'done') {

      if (info.file.response.success) {
        message.success(`файл ${info.file.name} успешно загружен`);

        window.g_app._store.dispatch({
          type: 'skGlobal/updateForm2Param',
          payload: {
            path: this.moduleData.get('path'),
            fieldName: this.configField.get('name'),
            fieldValue: info.file.response.file,
          }
        });

      }

    } else if (info.file.status === 'error') {
      message.error(`При загрузке файла ${info.file.name} произошла ошибка`);
    }
  };

  parseUrl = name => {
    const escapedName = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    const regexS = `[\\?&\\.]${escapedName}=([^&#\/]*)`;
    const regex = new RegExp( regexS );
    const results = regex.exec( window.location.href );
    if (results == null) {
      return '';
    }
    return results[1];
  };

  handlePopupButton = () => {

    const selectMode = 'fileBrowser';

    const section = this.parseUrl('section');
    const fieldName = this.configField.get('name');
    const path = this.moduleData.get('path');

    // собрать ссылку
    const href = `/oldadmin/?mode=${selectMode}&type=file&returnTo=fileSelector&section=${section}&fieldName=${fieldName}&path=${path}`;

    // открыть в новом окне
    sk.newWindow(href);

    return true;

  };

  getDecorator() {
    const { getFieldDecorator } = this.form;

    return getFieldDecorator(this.configField.get('name'), {
      skFieldInstance: this,
      initialValue: this.configField.get('value'),
      rules: [{ required: false, message: '' }],
    })(
      <Input disabled={!!this.configField.get('disabled')} />
    );
  }

  getFormFieldItem() {
    const decorator = this.getDecorator();

    const iSectionId = this.parseUrl('section');
    const folderAlias = this.serviceModuleData['_filebrowser_section'] || '';

    const props = {
      name: 'uploadFile[]',
      action: '/ajax/uploader.php',
      multiple: false,
      showUploadList: false,
      headers: {
        authorization: 'authorization-text',
      },
      data: {
        cmd: 'uploadImage',
        section: iSectionId,
        folder_alias: folderAlias,
      },

    };

    return (
      <Form.Item
        {...{
          labelCol: {span: 23},
          wrapperCol: {span: 24},
        }}
        key={this.configField.get('name')}
        help={<div dangerouslySetInnerHTML={{__html: this.configField.get('subtext')}}/>}
        label={this.configField.get('title')}
      >
        {decorator}
        <Button onClick={this.handlePopupButton}>
          {sk.dict('fileBrowserSelect')}
        </Button>
        <Upload
          {...props}
          onChange={this.handleOnChange(this)}
        >
          <Button>
            <Icon type="upload" /> {sk.dict('upload')}
          </Button>
        </Upload>

      </Form.Item>
    );
  }

}
