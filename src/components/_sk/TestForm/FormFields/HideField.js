import React from 'react';
import { Form, Input } from 'antd';
import FieldPrototype from './FieldPrototype';
import styles from '../Index.less';

export default class HideField extends FieldPrototype {
  getDecorator() {
    const { getFieldDecorator } = this.form;

    return getFieldDecorator(this.configField.get('name'), {
      skFieldInstance: this,
      initialValue: this.configField.get('value'),
      rules: [{ required: false, message: '' }],
    })(<Input type="hidden" />);
  }

  getFormFieldItem() {
    const decorator = this.getDecorator();

    return (
      <div className="sk-hide-field">
        <Form.Item
          key={this.configField.get('name')}
          help={<div dangerouslySetInnerHTML={{ __html: this.configField.get('subtext') }} />}
          label={this.configField.get('title')}
        >
          {decorator}
        </Form.Item>
      </div>
    );
  }
}
