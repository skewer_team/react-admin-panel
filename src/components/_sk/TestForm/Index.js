import React from 'react';
import { connect } from 'dva';
import { Divider, Form, Icon, Spin } from 'antd';
import CollapsiblePanels from '../CollapsiblePanels/Index';
import FieldsFactory from './FormFields/FieldsFactory';
import Styles from './Index.less';
import TestPanel from '../TestPanel/Index';
import * as sk from "../../../services/_sk/api";
import SkCanapeLoader from '../SkCanapeLoader/index';


const addText = (props) => {
  const { moduleData } = props;
  const text = moduleData.getIn(['params', 'addText']);

  return text ? (
    <div>
      <div dangerouslySetInnerHTML={{ __html: text }} />
      <Divider />
    </div>
  ) : (
    ''
  );
};

const handleClickOnButton = (props) => (configButton) => () => {

  const { moduleData, dispatch, form } = props;

  const {
    action='',
    state='',
    confirmText='',
    actionText=''
  } = configButton;

  if (action) {

    const formData = form.getFieldsValue();
    let handledFormData = {};

    Object.entries(formData).forEach(([name]) => {

      const {skFieldInstance} = form.getFieldProps(name)['data-__meta'];
      handledFormData = {
        ...handledFormData,
        [name]: skFieldInstance.getHandledValue()
      }
    });

    const data4Sending = {
      action,
      path: moduleData.get('path'),
      formData: handledFormData,
      from: 'form',
      configButton,
    };

    const dispatchConfig = {
      type: 'skGlobal/handleClickOnButton',
      payload: data4Sending,
    };

    switch (state) {
      case 'delete':

        let rowText = '';

        const item = form.getFieldsValue();

        if ( item )
          rowText = item.title || item.name || '';

        if ( !rowText )
          rowText = sk.dict('delRowNoName');

        const sHeader = sk.dict('delRowHeader');

        const sText = sk.dict('delRow').replace('{0}', rowText);

        sk.showModal(sHeader, sText, () => {
          dispatch(dispatchConfig);
        });

        break;


      case 'allow_do':

        sk.showModal(sk.dict('allowDoHeader'), actionText, () => {
          dispatch(dispatchConfig);
        });

        break;

      default:

        // Требуется подтверждение?
        if ( confirmText ) {

          sk.showModal(sk.dict('allowDoHeader'), confirmText, () => {
            dispatch(dispatchConfig);
          });

        } else {

          // Обычная отправка
          dispatch(dispatchConfig);

        }

    }
  }

};


export default
connect(({ skGlobal, loading }, { tabKey }) => {
  return {
    moduleData: skGlobal.tabs.get(tabKey),
    saveEffect: !!loading.effects['skGlobal/handleClickOnButton'],
  };
})(
  Form.create({
    name: 'horizontal_login',
    mapPropsToFields(props) {

      const {moduleData} = props;

      const mapNameToValue = {};

      moduleData.getIn(['params', 'items'], []).toJS().forEach(val => {
        mapNameToValue[val.name] = val.value;
      });

      return mapNameToValue;
    },
    onFieldsChange(props, changedValues, allValues) {
      const { moduleData, dispatch, form } = props;

      let handledFormData = {};

      // Собираем данные с полей
      Object.entries(allValues).forEach(([key]) => {

        const {skFieldInstance} = form.getFieldProps(key)['data-__meta'];
        handledFormData = {
          ...handledFormData,
          [key]: skFieldInstance.getHandledValue()
        };

      });

      // setFlagDirtyForm

      // Форма редактировалась?
      let formIsDirty = false;

      Object.entries(allValues).forEach(([nameField, configField]) => {
        if ( configField.value !== form.getFieldProps(nameField)['data-__meta']['initialValue'] ){
          formIsDirty = true;
        }
      });

      // Обновляем список грязных форм
      dispatch({
        type: 'skGlobal/updateDirtyFormsList',
        payload: {
          formName: moduleData.get('path'),
          isDirty: formIsDirty
        }
      });

      Object.entries(changedValues).forEach(([nameField, configField]) => {
        dispatch({
          type: 'skGlobal/updateForm',
          payload: {
            path: moduleData.get('path'),
            fieldName: nameField,
            fieldValue: configField.value,
            changedValues,
            values: handledFormData,
          },
        });
      });
    }
  })(
    (props) => {

      const { moduleData, saveEffect, form } = props;

      const getReactFieldByType = val => {
        return FieldsFactory.createFormField(
          val.get('type'),
          moduleData,
          {
            configField: val,
            form,
          }
        );
      };

      const fields = moduleData.getIn(['params', 'items'], []);

      const FormFields = fields.map(val => {
        const reactElem = getReactFieldByType(val, form);

        return Object.assign({}, { formItem: reactElem }, { data: val });
      });

      let groups = {};
      const elements = [];

      FormFields.forEach(value => {
        const groupTitle = value.data.get('groupTitle');

        if (groupTitle) {
          // Добавка приставки к числовым названиям групп, что бы js не отсортировал их автоматически
          const groupIndex = `g${groupTitle}`;

          // Добавляем группу, если её нет
          if (groups[groupIndex] === undefined) {
            groups = {
              ...groups,
              [groupIndex]: {
                groupTitle,
                collapsible: false,
                collapsed: false,
                items: []
              }
            };
          }

          groups[groupIndex].items.push(value.formItem);

          /* Добавление кнопки сворачивания группы, если хотя бы у одного поля определён параметр groupType */
          // Показ кнопки сворачивания группы
          groups[groupIndex].collapsible = value.data.get('groupType');
          // Состояние группы. True = свёрнута
          groups[groupIndex].collapsed = (value.data.get('groupType') > 1);
          // console.log(groups);
        } else {
          elements.push(value.formItem);
        }
      });

      elements.push(
        <CollapsiblePanels
          key="collapsiblePanel"
          groups={groups}
        />
      );


      const content = (
        <Spin
          tip="Загрузка..."
          // indicator={<Icon type="loading" style={{ fontSize: 32 }} spin />}
          indicator={<SkCanapeLoader fadein main={false} loadText={false} />}
          spinning={saveEffect}
        >
          <div className={Styles.form}>
            {addText(props)}
            <Form
              {...{
                hideRequiredMark: true,
                // labelCol: { span: 4 },
                // wrapperCol: { span: 20 },
                labelAlign: 'left',
                colon: false
              }}
            >
              {elements}
            </Form>
          </div>
        </Spin>
      );

      return (
        <TestPanel
          title={moduleData.getIn(['params', 'panelTitle'])}
          content={content}
          buttonsData={moduleData.getIn(['params', 'dockedItems', 'left'])}
          handleClickOnButton={handleClickOnButton(props)}
        />
      );

    }
  )
);
