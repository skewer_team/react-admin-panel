import React, { Component } from 'react';
import _ from 'lodash'
import classNames from 'classnames'
import UilScenery from '@iconscout/react-unicons/icons/uil-scenery';
import {Modal} from "antd";
import {connect} from 'dva'
import TestPanel from "../TestPanel/Index";
import styles from './Index.less'
import * as sk from "../../../services/_sk/api";


@connect()
class Index extends Component {

  state = {
    selectedItems: []
  };

  handleClick = (item) => (event) => {

    const { selectedItems } = this.state;

    // Зажата клавиша CTRL
    if (event.ctrlKey) {

      const indexInSelectedItems = _.findIndex(selectedItems, (elem) => {
        return elem.webPathShort === item.webPathShort
      });

      if (indexInSelectedItems !== -1) {
        // вычитаем элемент
        this.setState({
          selectedItems: _.differenceWith(selectedItems, [item], _.isEqual)
        })

      } else {
        // добавляем элемент
        this.setState({
          selectedItems: _.unionWith(selectedItems, [item], _.isEqual)
        });
      }

    } else {
      // Выбираем один элемент
      this.setState({
        selectedItems: [item]
      });
    }

  };

  buildContent = () => {

    const { moduleData } = this.props;

    const { selectedItems } = this.state;

    const haveImage = false;
    const image = (val) => {
      if (haveImage) {
        return <img src={val.preview} alt="" />
      }
      return (
        <div title="no image" className="sk-no-image">
          <UilScenery size="20" />
        </div>
      )
    };

    const files = moduleData.getIn(['params', 'files'])
      ? moduleData.getIn(['params', 'files']).toJS()
      : [];
    return files.length
      ? (
        <div className="list-files">
          {
            moduleData.getIn(['params', 'files']).toJS().map(val => {

              const isSelected = (
                _.findIndex(selectedItems, el => el.webPathShort === val.webPathShort) !== -1
              );

              return (
                <div
                  key={val.name}
                  className={
                    classNames("list-files__item", {
                      "list-files__item--selected": isSelected
                    })
                  }
                  onClick={this.handleClick(val)}
                >
                  {image(val)}
                  <div className="list-files__title" title={val.name}>
                    {val.name}
                  </div>
                </div>
              )
            })
          }
        </div>
      ) : null;
  };

  handleClickOnButton = (moduleData) => (configButton) => () => {

    const {state = ''} = configButton;

    const {selectedItems} = this.state;

    const {dispatch} = this.props;

    switch (state) {

      case 'delete':

        // если не выбрано - выдать ошибку
        if ( !selectedItems.length ){
            sk.error(moduleData.getIn(['init','lang', 'fileBrowserNoSelection']));
          return false;
        }

        // задание текста для подтверждения
        let rowText;

        if ( selectedItems.length === 1 ){
          rowText = _.head(selectedItems).name || '';

          if ( rowText ){
            rowText = `"${rowText}"`;
          } else {
            rowText = moduleData.getIn(['init', 'lang', 'delRowNoName']);
          }

        } else {

          rowText = `${selectedItems.length.toString()} ${moduleData.getIn(['init', 'lang', 'delCntItems'])}`;

        }

        sk.showModal(sk.dict('delRowHeader'), sk.dict('delRow').replace('{0}', rowText), () => {

          const delItems = selectedItems.map(item => item.name);

          const dataPack = moduleData.getIn(['params', 'serviceData']).toJS();

          const componentData = {
            cmd: 'delete',
            delItems
          };

          dispatch({
            type: 'skGlobal/deleteFileItems',
            payload: {
              path: moduleData.get('path'),
              data: {
                ...dataPack,
                ...componentData
              }
            }
          });

          // чистим state
          this.setState({
            selectedItems: []
          });

        });

        break;

      case 'copy_filelink':

        if (selectedItems.length === 0){
          sk.error(moduleData.getIn(['init','lang','chooseFile']));
          return false;
        }

        const items = selectedItems.map((item) =>
          <p key={item.name}>{item.webPathShort}</p>
        );

        Modal.info({
          title: moduleData.getIn(['init', 'lang', 'showFilesLink']),
          content: (
            <div>
              {items}
            </div>
          ),
          //
          onOk() {
          },
        });

        break;

      default:

        dispatch({
          type: 'skGlobal/filesAddForm',
          payload: {
            path: moduleData.get('path')
          }
        });

    }

    return true;

  };

  render() {

    const { moduleData } = this.props;

    return (
      <TestPanel
        title={moduleData.getIn(['params', 'panelTitle'])}
        content={this.buildContent()}
        buttonsData={moduleData.getIn(['params', 'dockedItems', 'left'])}
        handleClickOnButton={this.handleClickOnButton(moduleData)}
      />
    );
  }
}

export default Index;
