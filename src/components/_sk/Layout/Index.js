import React, { Component } from 'react';
import { Layout } from "antd";
import logo from "../../../assets/logo.svg";
import styles from "../../../layouts/_sk/MainLayout.less";
import SiderMenuDrawer from '../SiderMenu/SiderMenuDrawer';
import Header from '../Header/Index';
import Footer from '../../../layouts/_sk/Footer'
import SiderCollapseButton from '../SiderCollapseButton/Index'



export default class Index extends Component {
  
  constructor(props) {
    super(props);
    this.state = { collapsed: false }
    this.handleClick = this.handleClick.bind(this);
  }

  handleMenuCollapse = props => collapsed => {
    const { dispatch } = props;
    dispatch({
      type: 'skGlobal/changeLayoutCollapsed',
      payload: collapsed,
    });
    
  };
  
  handleClick() {
    this.setState(state => ({
      collapsed: !state.collapsed
    }));
  }

  render() {
    const {
      navTheme,
      layout: PropsLayout,
      children,
      isMobile,
      fixedHeader,
    } = this.props;
  
    const isTop = PropsLayout === 'topmenu';
    const contentStyle = !fixedHeader ? { paddingTop: 0 } : {};
    const { collapsed } = this.state

    return (
      <Layout hasSider={false}>
        <Layout hasSider>
          {isTop && !isMobile ? null : (
            <SiderMenuDrawer
              logo={logo}
              theme={navTheme}
              collapsed={collapsed}
              onCollapse={this.handleMenuCollapse(this.props)}
              isMobile={isMobile}
              {...this.props}
            />
          )}
          <Layout
            style={{
              minHeight: '100vh',
            }}
          >
            <SiderCollapseButton onClick={this.handleClick} collapsed={collapsed} />
            <Header />
  
            <Layout.Content className={styles.content} style={contentStyle}>
              {children}
            </Layout.Content>
          </Layout>
  
        </Layout>
  
        <Footer />
  
      </Layout>
    );
  }
}


