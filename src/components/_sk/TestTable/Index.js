import React, { Fragment } from 'react';
import { Form, Modal, Table } from 'antd';

import { connect } from 'dva';
import _ from 'lodash';
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import update from 'immutability-helper';
import moment from 'moment';
import classNames from 'classnames';
import TableRow from './TableRow'
import ButtonInTableRow from './ButtonInTableRow/Index';
import TestPanel from '../TestPanel/Index';
import DragableBodyRow from './DraggableRowDecorator/Index';
import { EditableCell, EditableDecorator } from './EditableRowDecorator/Index';
import FilterForm from './FilterForm/Index';
import ParamsAddObjBtn from './CustomButtonsInTableRow/Params/ParamsAddObjBtn/Index';
import ParamsEditBtn from './CustomButtonsInTableRow/Params/ParamsEditBtn/Index';
import ParamsDelBtn from './CustomButtonsInTableRow/Params/ParamsDelBtn/Index';
import ApproveBtn from './CustomButtonsInTableRow/ZonesEditor/ApproveBtn/Index';
import DeleteOrCloneBtn from './CustomButtonsInTableRow/ZonesEditor/DeleteOrCloneBtn/Index';
import DelBtn from './CustomButtonsInTableRow/Languages/DelBtn/Index';
import OverrideBtn from './CustomButtonsInTableRow/Languages/OverrideBtn/Index';
import EditBtn from './CustomButtonsInTableRow/Languages/EditBtn/Index';
import StatusGroupBtn from './CustomButtonsInTableRow/Languages/StatusGroupBtn/Index';
import AuthStatusGroupBtn from './CustomButtonsInTableRow/Auth/StatusGroupBtn/Index';
import TranslateBtn from './CustomButtonsInTableRow/Languages/TranslateBtn/Index';
import ReviewApproveBtn from './CustomButtonsInTableRow/Review/ApproveBtn/Index'
import ReviewRejectBtn from './CustomButtonsInTableRow/Review/RejectBtn/Index'
import RegionsParamsCleanObjBtn from './CustomButtonsInTableRow/Regions/ParamsCleanObjBtn/Index'
import CatalogViewModificationsBtn from './CustomButtonsInTableRow/Catalog/ViewModificationsBtn/Index'
import CardEditorDelFieldBtn from './CustomButtonsInTableRow/CardEditor/DelFieldBtn/Index'
import CardEditorEditFieldBtn from './CustomButtonsInTableRow/CardEditor/EditFieldBtn/Index'
import * as sk from "../../../services/_sk/api";
import styles from "./index.less"
import Exception from "../../Exception";

const getModuleNameByCustomBtnName = (subList, customButtonName) => {

  const indexLib = subList.findIndex(lib => {
    return lib.name === customButtonName
  });

  if ( indexLib === -1 ){
    throw new Error('Unknown lib');
  }

  return subList[indexLib].module;

};

/**
 * Построит путь к модулю кастомной кнопки по её названию
 */
const buildCustomBtnModulePathByName = (subList, customButtonName) => {

  const indexLib = subList.findIndex(lib => {
    return lib.name === customButtonName
  });

  if ( indexLib === -1 ){
    throw new Error('Unknown lib');
  }

  //   library structure
  //   name: 'ParamsAddObjBtn', layer: 'Adm', module: 'Params', dir: '/assets/5c312e79', notOwn: false,
  const library = subList[indexLib];

  return `CustomButtonsInTableRow/${library.module}/${library.name}/Index.js`;

};

const handleSave = (props) => (row, fieldName, listSaveCmd) => {
  const { dispatch, moduleData } = props;

  const data4Sending = {
    path: moduleData.get('path'),
    cmd: listSaveCmd,
    from: 'field',
    data: row,
    fieldName,
  };

  dispatch({
    type: 'skGlobal/saveFieldTableFromList',
    payload: data4Sending,
  });
};


/**
 * Обработка клика по кнопке в табличной строке
 * @param props - props из родительского модуля
 */
const handleClickOnButtonRow = (props) => (configButton, tableRecord) => (e) => {

  if ( e ){
    // Отменяем всплытие события, если оно передано
    e.stopPropagation();
  }

  // { tooltip: "Редактировать", iconCls: "icon-edit", action: "show", state: "edit_form" }
  const {action, state, actionText=''} = configButton;

  const { moduleData, dispatch } = props;

  // Если action не указан, то ничего не делаем
  if ( !action ){
    return null;
  }

  const data4Sending = {
    path: moduleData.get('path'),
    cmd: action,
    data: tableRecord,
  };

  const postData = () => {
    dispatch({
      type: 'skGlobal/handleClickOnButtonTable',
      payload: data4Sending,
    });
  };

  switch (state) {

    // удалить
    case 'delete':

      let rowText = tableRecord.title || tableRecord.name;

      if (!rowText){

        let titleField;
        const tableColumns = this.columns.toJS();

        for (const index in tableColumns){
          if ( tableColumns[index] && tableColumns[index]['dataIndex'] ){
            titleField = tableColumns[index]['dataIndex'];
            break;
          }
        }

        if ( titleField ){
          rowText = tableRecord[titleField];
        }

      }

      const header = sk.dict('delRowHeader');
      const text = sk.dict('delRow').replace('{0}', rowText);

      sk.showModal(header, text, () => {
        postData();
      });

      break;

    // Действия, требующие подтверждения выполнения операции
    case 'allow_do':

      sk.showModal(sk.dict('allowDoHeader'), actionText, () => {
        postData();
      });

      break;

    default:
      postData();
  }

};

/**
 * Построит массив кнопок для табличной строки
 */
const buildButtonsForTableRow = (props) => (text, record) => {

  const {moduleData,dispatch} = props;

  // Кнопки в строке
  const rowButtons = moduleData.getIn(['params', 'rowButtons']);

  // Если есть дочерние записи, значит это группирующая строка для которой кнопки не нужны
  if (record.children !== undefined){
    return false;
  }

  return rowButtons.toJS().map(rowButtonVal => {

    // { tooltip: "Удалить", iconCls: "icon-delete", action: "delete", state: "delete", actionText: "" }
    // {state: "add_obj", customBtnName: "ParamsAddObjBtn", customLayer: ""}
    const configButton = rowButtonVal;

    const subList = moduleData.get('subLibs').toJS();

    let button;

    // Это кастомная кнопка?
    if ( configButton.customBtnName !== undefined && configButton.customLayer !== undefined ){

      const customButtonData = {
        record,
        mainContainerData: moduleData,
        dispatch,
        configButton
      };

      switch (configButton.customBtnName) {
        case 'ParamsAddObjBtn':
          button = <ParamsAddObjBtn {...customButtonData} />;
          break;

        case 'ParamsEditBtn':
          button = <ParamsEditBtn {...customButtonData} />;
          break;

        case 'ParamsDelBtn':
          button = <ParamsDelBtn {...customButtonData} />;
          break;

        case 'ApproveBtn':

          switch (getModuleNameByCustomBtnName(subList, 'ApproveBtn')){
            case 'Review':
              button = <ReviewApproveBtn {...customButtonData} />;
              break;

            default:
              button = <ApproveBtn {...customButtonData} />;
          }

          break;

        case 'DeleteOrCloneBtn':
          button = <DeleteOrCloneBtn {...customButtonData} />;
          break;

        case 'DelBtn':
          button = <DelBtn {...customButtonData} />;
          break;

        case 'OverrideBtn':
          button = <OverrideBtn {...customButtonData} />;
          break;

        case 'EditBtn':
          button = <EditBtn {...customButtonData} />;
          break;

        case 'StatusGroupBtn':

          switch ( getModuleNameByCustomBtnName(subList, 'StatusGroupBtn') ){
            case 'Auth':
              button = <AuthStatusGroupBtn {...customButtonData} />;
              break;

            default:
              button = <StatusGroupBtn {...customButtonData} />;
          }

          break;

        case 'TranslateBtn':
          button = <TranslateBtn {...customButtonData} />;
          break;

        case 'RejectBtn':
          button = <ReviewRejectBtn {...customButtonData} />;
          break;

        case 'ParamsCleanObjBtn':
          button = <RegionsParamsCleanObjBtn {...customButtonData} />;
          break;

        case 'ViewModificationsBtn':
          button = <CatalogViewModificationsBtn {...customButtonData} />;
          break;

        case 'DelFieldBtn':
          button = <CardEditorDelFieldBtn {...customButtonData} />;
          break;

        case 'EditFieldBtn':
          button = <CardEditorEditFieldBtn {...customButtonData} />;
          break;

        default:
          button = <ParamsAddObjBtn {...customButtonData} />;
        // throw new Exception('unknown custom button');
      }

    } else {
      // Стандартная кнопка

      button = (
        <ButtonInTableRow
          // key={index}
          configButton={configButton}
          handleClickOnButton={handleClickOnButtonRow(props)(configButton, record)}
        />
      );

    } // else


    return button;

  });

};


const initColumns = (props) => {

  let outColumns = [];

  const { moduleData } = props;

  // Колонки
  const columns = moduleData.getIn(['params', 'columnsModel']);

  // Кнопки в строке
  const rowButtons = moduleData.getIn(['params', 'rowButtons']);

  // сумма flex колонок
  const sumFlex = columns.reduce((sum, val) => {
    const curVal = val.get('flex') || 0;
    return sum + curVal;
  }, 0);

  const container = document.querySelector(".sk-tabs");

  // общая ширина контейнера для вывода колонок
  const sumWidth = container ? container.clientWidth - 210 - 41 : 782;

  // ширина колонки под кнопки
  const buttonsWidth = rowButtons.size * 18 + 18;
  const buttonsWidthPercent = Math.round(100 * buttonsWidth / sumWidth);

  // считаем остаток ширины для flex колонок (убираем все с фиксированной
  // шириной и колонку с кнопками)
  let restWidth = sumWidth - buttonsWidth;
  restWidth = columns.reduce((rest, val) => {
    const width = val.get('width') || 0;
    if (width) {
      return rest - width;
    }
    return rest;
  }, restWidth);

  outColumns = columns.map(value => {
    const flex = value.get('flex') || 0;

    // % от ширины поля
    let width;

    const baseWidth = value.get('width') || 0;
    if (baseWidth) {
      width = Math.round(100 * baseWidth / sumWidth);
    } else {
      width = Math.round(100 * restWidth * flex / sumFlex / sumWidth);
    }

    let column = {
      title: value.get('text'),
      dataIndex: value.get('dataIndex'),
      align: value.get('jsView') === 'check' ? 'center' : 'left', // todo
      width: `${width}%`,
      render: (text, record) => {
        return <div dangerouslySetInnerHTML={{ __html: record[value.get('dataIndex')] }} />
      }
    };

    if (value.get('sortable')) {
      column = {
        ...column,
        sorter: (a, b) => {
          const fieldSort = value.get('dataIndex');

          const typeField = value.get('jsView');

          let res;
          switch (typeField) {
            case 'num':
            case 'money':
            case 'float':
              res = a[fieldSort] - b[fieldSort];
              break;

            default:
              res = a[fieldSort] > b[fieldSort] ? 1 : -1;
          }

          // сравнение строк
          return res;
        },
      };
    }

    if (value.get('listSaveCmd')) {
      // console.log(value.get('jsView'))
      column = {
        ...column,
        editable: true,
        onCell: record => ({
          record,
          listSaveCmd: value.get('listSaveCmd'),
          jsView: value.get('jsView'),
          editable: true,
          dataIndex: value.get('dataIndex'),
          title: value.get('text'),
          handleSave: handleSave(props),
          width: `${width}%`,
        }),
      };
    }

    return column;
  });

  // Добавляем кнопки в строку
  outColumns = outColumns.push({
    title: '',
    dataIndex: '__buttonsInRow__',
    width: `${buttonsWidthPercent}%`,
    align: 'right',
    render: buildButtonsForTableRow(props),
  });

  outColumns = outColumns.map((col, index) => ({
    ...col,
    onHeaderCell: column => {
      return {
        width: column.width,
        // onResize: this.handleResize(index),
      };
    },
  }));

  return outColumns.toJS();
};

const initDataSource = (columns, props) => {

  const { moduleData } = props;

  // Данные
  const data = moduleData.getIn(['params', 'items']);

  // поле группировки записей
  const groupField = moduleData.getIn(['params', 'init', 'groupslist_groupField']) || '';

  if (groupField) {
    const recordsIndexedByGroup = _.groupBy(data.toJS(), groupField);

    const dataRecords = [];

    Object.entries(recordsIndexedByGroup).forEach(([nameGroup, records]) => {
      const titleGroup = columns[0].dataIndex;

      const children = records.map(value => {
        return {
          ...value,
          key: value.id,
        };
      });

      const groupWithChildRecords = {
        key: nameGroup,
        [titleGroup]: nameGroup,
        children,
      };

      dataRecords.push(groupWithChildRecords);
    });

    return dataRecords;
  }

  return data.toJS().map(value => {
    return {
      ...value,
      key: value.id,
    };
  });
};

@connect(({ skGlobal, loading }, { tabKey }) => {
  return {
    moduleData: skGlobal.tabs.get(tabKey),
    saveEffect:
      !!loading.effects['skGlobal/handleFilterButton'] ||
      !!loading.effects['skGlobal/handleFilterData'],
  };
})
class Index extends React.Component {
  constructor(props) {
    super(props);

    const columns = initColumns(props);
    const dataSource = initDataSource(columns, props);

    this.state = {
      filterValues: [], // this.initFilterValues(),
      selectedRowKeys: [],
      selectedRows: [],
      columns,
      dataSource,
      components: [], // this.initComponents(),
    };
  }

  componentWillReceiveProps(nextProps, nextContext) {

    const oldColumns = initColumns(this.props);
    const oldData = initDataSource(oldColumns, this.props);

    const newColumns = initColumns(nextProps);
    const newData = initDataSource(newColumns, nextProps);

    if ( !_.isEqual(oldData, newData) ){
      this.setState({
        dataSource: newData
      })
    }

  }

  initFilterValues = () => {
    const out = {};

    const { moduleData } = this.props;

    const filters = moduleData.getIn(['params', 'barElements']);

    filters.toJS().forEach(filter => {
      switch (filter.libName) {
        case 'Ext.Builder.ListFilterText':
          out[filter.fieldName] = filter.fieldValue;

          break;

        case 'Ext.Builder.ListFilterSelect':
          const checkedIndex = filter.menu.items.findIndex(value => {
            return !!value.checked;
          });

          const checkedValue = checkedIndex !== -1 ? filter.menu.items[checkedIndex].data : false;
          out[filter.fieldName] = checkedValue;

          break;

        case 'Ext.Builder.ListFilterDate':
          const dateFormat = 'YYYY/MM/DD';

          const begin = filter.fieldValue[0] ? moment(filter.fieldValue[0], dateFormat) : '';
          const end = filter.fieldValue[1] ? moment(filter.fieldValue[1], dateFormat) : '';

          out[filter.fieldName] = [begin, end];

          break;

        default:
          break;
      }
    });

    return out;
  };

  /**
   * Клик по кнопкам левой панели
   * */
  handleClickOnButton = (configButton) => () => {
    const { moduleData, dispatch } = this.props;

    const { selectedRows } = this.state;

    const {
      action='',
      state='',
      confirmText='',
      actionText=''
    } = configButton;

    const data = {
      items: selectedRows,
      multiple: true,
    };

    if (action) {
      const data4Sending = {
        action,
        path: moduleData.get('path'),
        formData: data,
        from: 'list',
        configButton
      };

      const dispatchConfig = {
        type: 'skGlobal/handleClickOnButton',
        payload: data4Sending,
      };

      switch (state) {
        case 'delete':

          let rowText = '';

          let item = [];

          if ( (typeof(data.items) === 'object')&& data.items.length == 1 )
            item = data.items[0];

          if ( item )
            rowText = item.title || item.name || '';

          if ( !rowText )
            rowText = sk.dict('delRowNoName');

          let sHeader = sk.dict('delRowHeader');

          if ( moduleData.getIn(['params', 'checkboxSelection']) ){
            if (selectedRows.length > 1){
              sHeader = sk.dict('delRowsHeader');
              rowText = sk.dict('delRowsNoName');
            }
          }

          const sText = sk.dict('delRow').replace('{0}', rowText);

          sk.showModal(sHeader, sText, () => {
            dispatch(dispatchConfig);
          });

          break;

        case 'allow_do':
          sk.showModal(sk.dict('allowDoHeader'), actionText, () => {
            dispatch(dispatchConfig);
          });

          break;

        case 'popup_window':

          const {url} = configButton;
          sk.newWindow( url );

          break;

        default:

          // Требуется подтверждение?
          if ( confirmText ) {

            sk.showModal(sk.dict('allowDoHeader'), confirmText, () => {
              dispatch(dispatchConfig);
            });

          } else {

            // Обычная отправка
            dispatch(dispatchConfig);

          }
      }

    }

  };


  changeFilterForm = form => () => {
    const { dispatch, moduleData } = this.props;

    // console.log(moduleData, "module data");
    const formValues = form.getFieldsValue();

    this.setState({
      filterValues: formValues,
    });

    dispatch({
      type: 'skGlobal/handleFilterData',
      payload: {
        path: moduleData.get('path'),
        page: 0,
        values: {
          cmd: moduleData.getIn(['params', 'actionNameLoad']),
          ...formValues,
        },
      },
    });
  };

  onSelectChange = (selectedRowKeys, selectedRows) => {
    this.setState({ selectedRowKeys, selectedRows });
  };

  initCheckBoxSelection = () => {
    const { moduleData } = this.props;

    let config = {};

    if (moduleData.getIn(['params', 'checkboxSelection'])) {
      const { selectedRowKeys } = this.state;

      const rowSelection = {
        selectedRowKeys,
        onChange: this.onSelectChange,
      };

      config = {
        rowSelection,
      };
    }

    return config;
  };

  initPagination = () => {
    const { moduleData, dispatch } = this.props;

    const total = moduleData.getIn(['params', 'itemsTotal']);
    const onPage = moduleData.getIn(['params', 'itemsOnPage']) || 0;
    const pageNum = (moduleData.getIn(['params', 'pageNum']) || 0) + 1;

    let config = {
      pagination: false,
    };

    if (onPage) {
      config = {
        pagination: {
          onChange: page => {
            const { filterValues } = this.state;

            dispatch({
              type: 'skGlobal/handleFilterData',
              payload: {
                path: moduleData.get('path'),
                page: page-1,
                values: {
                  cmd: moduleData.getIn(['params', 'actionNameLoad']),
                  ...filterValues,
                },
              },
            });
          },
          showQuickJumper: true,
          total,
          showTotal: (totalCount, range) => `${range[0]}-${range[1]} из ${totalCount} записей`,
          pageSize: onPage,
          defaultCurrent: pageNum,
        },
      };
    } // end if

    return config;
  };


  initOnRowEvents = () => {
    return {
      onRow: (record, rowIndex) => {
        return {
          index: rowIndex,
          moveRow: this.moveRow,
          onClick: this.onClickByRow(record, rowIndex),
          onDoubleClick: this.onDoubleClickByRow(record, rowIndex),
        };
      },
    };
  };

  onClickByRow = (record, rowIndex) => event => {
    this.selectRow(record);
  };

  onDoubleClickByRow = (record, rowIndex) => event => {
    const { moduleData } = this.props;

    // Если есть дочерние записи, значит это группирующая строка
    if (record.children !== undefined){
      return false;
    }

    // console.log('onDoubleClick', record, rowIndex, event);

    // Кнопки в строке
    const rowButtons = moduleData.getIn(['params', 'rowButtons']).toJS();

    // console.log('rowButtons', rowButtons);

    // найти кнопку редактирования
    const indexButton = rowButtons.findIndex(value => {
      return value.state === 'edit_form';
    });

    if (indexButton !== -1) {
      const targetButton = rowButtons[indexButton];

      // Это кастомная кнопка?
      if ( targetButton.customBtnName !== undefined ){

        const {dispatch} = this.props;

        const subList = moduleData.get('subLibs').toJS();
        const modulePath = buildCustomBtnModulePathByName(subList, targetButton.customBtnName);

        // Вызываем её обработчик
        import(`./${modulePath}`)
          .then(module => {
            module.handler({
              record,
              mainContainerData: moduleData,
              dispatch,
              configButton: targetButton
            });
          })
          .catch(err => {
            console.log('err', err);
            // main.textContent = err.message;
          });



      } else {

        // стандартная кнопка

        // console.log('standard button', targetButton.action, targetButton.state, record);
        handleClickOnButtonRow(this.props)(targetButton, record)();

      }

    }
  };

  handleResize = index => (e, { size }) => {
    this.setState(({ columns }) => {
      // console.log(index, size);

      const nextColumns = [...columns];
      nextColumns[index] = {
        ...nextColumns[index],
        width: size.width,
      };
      return { columns: nextColumns };
    });
  };

  initComponents = (columns) => {
    const indexFirstEditableCell = columns.findIndex(val => !!val.editable);

    // Таблица редактируемая?
    const isEditableTable = indexFirstEditableCell !== -1;

    const components = {};

    let row = TableRow;
    let cell;

    const { moduleData } = this.props;

    if (moduleData.getIn(['params', 'ddAction'])) {
      row = DragableBodyRow;
    }
    //
    // if (isEditableTable) {
    //   cell = EditableCell;
    //   row = Form.create()(EditableDecorator(row));
    //   // todo Form.create должен лежать в самом декораторе
    // }

    if (cell) {
      components.body = {
        ...components.body,
        cell,
      };
    }

    if (row) {
      components.body = {
        ...components.body,
        row,
      };
    }

    return components;
  };

  moveRow = (dragIndex, hoverIndex) => {

    const { dataSource } = this.state;
    const {moduleData, dispatch} = this.props;

    const dragRow = dataSource[dragIndex];

    const direction = dragIndex < hoverIndex ? 'after' : 'before';

    dispatch({
      type: 'skGlobal/sortTableItems',
      payload: {
        path: moduleData.get('path'),
        dragItem: dataSource[dragIndex],
        hoverItem: dataSource[hoverIndex],
        direction
      }
    });

    this.setState(
      update(this.state, {
        dataSource: {
          $splice: [[dragIndex, 1], [hoverIndex, 0, dragRow]],
        },
      })
    );
  };

  selectRow = record => {
    const { selectedRowKeys } = this.state;

    const aSelectedRowKeys = [...selectedRowKeys];
    if (aSelectedRowKeys.indexOf(record.key) >= 0) {
      aSelectedRowKeys.splice(aSelectedRowKeys.indexOf(record.key), 1);
    } else {
      aSelectedRowKeys.push(record.key);
    }
    this.setState({ selectedRowKeys: aSelectedRowKeys });
  };

  render() {
    const { tabKey, saveEffect, moduleData } = this.props;

    const filterValues = this.initFilterValues();
    const columns = initColumns(this.props);
    const components = this.initComponents(columns);

    const {dataSource} = this.state;

    // const { dataSource, columns, components } = this.state;

    const config = {
      components,
      columns,
      dataSource,
      rowClassName: () => 'editable-row',
      bordered: true,
      loading: saveEffect,
      // scroll={{y: true}}
      size: 'small',
      indentSize: 0,
      defaultExpandAllRows: true,
      ...this.initCheckBoxSelection(),
      ...this.initPagination(),
      ...this.initOnRowEvents(),
    };

    const content = (
      <Fragment>
        {Object.entries(filterValues).length === 0 && filterValues.constructor === Object ? (
          null
        ) : (
          <FilterForm tabKey={tabKey} changeFilterForm={this.changeFilterForm} />
        )}
        <Table {...config} />
      </Fragment>
    );

    return (
      <TestPanel
        title={moduleData.getIn(['params', 'panelTitle'])}
        content={content}
        buttonsData={moduleData.getIn(['params', 'dockedItems', 'left'])}
        handleClickOnButton={this.handleClickOnButton}
      />
    );
  }
}

export default DragDropContext(HTML5Backend)(Index);
