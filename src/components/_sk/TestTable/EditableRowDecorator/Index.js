import React from 'react';
import { Input, Checkbox, Form, InputNumber } from 'antd';

const FormItem = Form.Item;

const EditableContext = React.createContext();

const EditableRow = ({ form, index, ...props }) => {
  return (
    <EditableContext.Provider value={form}>
      <tr {...props} />
    </EditableContext.Provider>
  );
};

const EditableDecorator = OriginalComponent => ({ form, ...props }) => {
  return (
    <EditableContext.Provider value={form}>
      <OriginalComponent {...props} />
    </EditableContext.Provider>
  );
};

class EditableCell extends React.Component {
  state = {
    editing: false,
  };

  toggleEdit = () => {
    const { editing } = this.state;

    const bEditing = !editing;
    this.setState({ editing: bEditing }, () => {
      if (bEditing) {
        this.input.focus();
      }
    });
  };

  save = fieldName => () => {
    const { record, handleSave, listSaveCmd } = this.props;

    this.form.validateFields((error, values) => {
      if (error) {
        return;
      }

      handleSave({ ...record, ...values }, fieldName, listSaveCmd, this.form);
      this.toggleEdit();
    });
  };

  render() {
    const { editing } = this.state;

    const { width, jsView, editable, dataIndex, record, ...restProps } = this.props;

    const getTitle = () => {
      const { children } = this.props
      const title = children[2]
      if (typeof(title) === "string") {
        return title
      }
      return null
    }
    
    const title = getTitle();

    return (
      <td title={title} {...restProps} style={{ maxWidth: width }}>
        {editable ? (
          <EditableContext.Consumer>
            {form => {
              this.form = form;

              let elem;

              if (editing) {
                switch (jsView) {
                  case 'check':
                    elem = (
                      <FormItem style={{ margin: 0 }}>
                        {form.getFieldDecorator(dataIndex, {
                          rules: [{ required: false, message: `` }],
                          valuePropName: 'checked',
                          initialValue: record[dataIndex] !== '0' && record[dataIndex],
                        })(
                          <Checkbox
                            ref={node => (this.input = node)}
                            onPressEnter={this.save(dataIndex)}
                            className="sk-table_checkbox"
                            onBlur={this.save(dataIndex)}
                          />
                        )}
                      </FormItem>
                    );

                    break;

                  case 'str':
                    elem = (
                      <FormItem style={{ margin: 0 }}>
                        {form.getFieldDecorator(dataIndex, {
                          rules: [{ required: false, message: `` }],
                          initialValue: record[dataIndex],
                        })(
                          <Input
                            ref={node => (this.input = node)}
                            onPressEnter={this.save(dataIndex)}
                            onBlur={this.save(dataIndex)}
                            title={this.value}
                          />
                        )}
                      </FormItem>
                    );

                    break;

                  case 'text':
                  case 'text_html':
                  case 'text_js':
                  case 'text_css':
                    elem = (
                      <FormItem style={{ margin: 0 }}>
                        {form.getFieldDecorator(dataIndex, {
                          rules: [{ required: false, message: `` }],
                          initialValue: record[dataIndex],
                        })(
                          <Input.TextArea
                            rows={1}
                            ref={node => (this.input = node)}
                            onPressEnter={this.save(dataIndex)}
                            onBlur={this.save(dataIndex)}
                            title={this.value}
                          />
                        )}
                      </FormItem>
                    );

                    break;

                  case 'money':
                  case 'float':
                    elem = (
                      <FormItem style={{ margin: 0 }}>
                        {form.getFieldDecorator(dataIndex, {
                          rules: [{ required: false, message: `` }],
                          initialValue: record[dataIndex],
                        })(
                          <InputNumber
                            ref={node => (this.input = node)}
                            onPressEnter={this.save(dataIndex)}
                            onBlur={this.save(dataIndex)}
                            min={0}
                            max={10}
                            step={0.1}
                            title={this.value}
                          />
                        )}
                        
                      </FormItem>
                    );

                    break;

                  case 'num':
                    elem = (
                      <FormItem style={{ margin: 0 }}>
                        {form.getFieldDecorator(dataIndex, {
                          rules: [{ required: false, message: `` }],
                          initialValue: record[dataIndex],
                        })(
                          <InputNumber
                            ref={node => (this.input = node)}
                            onPressEnter={this.save(dataIndex)}
                            onBlur={this.save(dataIndex)}
                            title={this.value}
                          />
                        )}
                      </FormItem>
                    );

                    break;

                  default:
                }
              } else {
                switch (jsView) {
                  case 'check':
                    elem = (
                      <FormItem style={{ margin: 0 }}>
                        {form.getFieldDecorator(dataIndex, {
                          rules: [{ required: false, message: `` }],
                          valuePropName: 'checked',
                          initialValue: record[dataIndex] !== '0' && record[dataIndex],
                        })(
                          <Checkbox
                            ref={node => (this.input = node)}
                            className="sk-table_checkbox"
                            onChange={this.save(dataIndex)}
                          />
                        )}
                        
                      </FormItem>
                    );

                    break;

                  case 'str':
                  default:
                    elem = (
                      <div
                        className="editable-cell-value-wrap"
                        // style={{ paddingRight: 24 }}
                        onClick={this.toggleEdit}
                      > 
                        {restProps.children}
                      </div>
                    );
                }
              }

              return elem;
            }}
          </EditableContext.Consumer>
        ) : (
            restProps.children
          )}
      </td>
    );
  }
}

export { EditableCell, EditableDecorator };
