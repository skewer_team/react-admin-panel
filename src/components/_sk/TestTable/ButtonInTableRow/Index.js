import React from 'react';
import {Tooltip} from "antd";
import Icon from '../../Icon/Index'

export default (props) => {

  const { configButton, handleClickOnButton } = props;

  if ( !configButton )
    return null;

  return (
    <Tooltip
      placement="bottomRight"
      title={configButton.tooltip}
    >
      <span>
        <Icon
          alias={configButton.iconCls}
          addProps={{onClick: handleClickOnButton}}
        />
      </span>
    </Tooltip>
  );

};
