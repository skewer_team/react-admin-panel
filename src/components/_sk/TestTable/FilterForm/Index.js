import React, { Component } from 'react';
import { Button, Form, Input, Select, DatePicker } from 'antd';
import { connect } from 'dva';
import classNames from 'classnames';
import moment from 'moment';
import {debounce} from 'lodash';
// import { doc } from 'prettier';

@connect(({ skGlobal }, { tabKey }) => {
  return {
    moduleData: skGlobal.tabs.get(tabKey),
    // saveEffect: !!loading.effects['skGlobal/handleClickOnButton']
  };
})
@Form.create()
class Index extends Component {
  state = {
    collapse: false
  };

  componentDidMount() {
    document.addEventListener("click", () => {
      const sidebarWidth = document.getElementsByClassName('ant-layout-sider')[0].style.width
      this.setState({
        collapse: false
      });

      if (sidebarWidth < 310) {
        this.setState({
          collapse: true
        })
      }
    })
  }


  handleFilterButton = addParams => () => {
    const { moduleData, dispatch } = this.props;

    dispatch({
      type: 'skGlobal/handleFilterButton',
      payload: {
        path: moduleData.get('path'),
        addParams,
      },
    });
  };

  render() {
    const { moduleData, form, changeFilterForm } = this.props;

    const filters = moduleData.getIn(['params', 'barElements']);

    const { getFieldDecorator } = form;

    const { collapse } = this.state;

    const filterFields = filters.toJS().map(filter => {
      let field = {};

      switch (filter.libName) {
        case 'Ext.Builder.ListFilterText':
          field = (
            <Form.Item>
              {getFieldDecorator(filter.fieldName, {
                initialValue: filter.fieldValue,
              })(
                <Input
                  allowClear
                  placeholder={filter.emptyText}
                  onPressEnter={debounce(changeFilterForm(form), 50)}
                />
              )}
            </Form.Item>
          );

          break;

        case 'Ext.Builder.ListFilterSelect':
          const checkedIndex = filter.menu.items.findIndex(value => {
            return !!value.checked;
          });

          const checkedValue = checkedIndex !== -1 ? filter.menu.items[checkedIndex].data : false;
          const options = filter.menu.items.map(value => {
            return (
              <Select.Option key={value.data} value={value.data}>
                <div dangerouslySetInnerHTML={{ __html: value.text }} />
              </Select.Option>
            );
          });

          field = (
            <Form.Item label={filter.text}>
              {getFieldDecorator(filter.fieldName, {
                initialValue: checkedValue,
              })(
                <Select
                  dropdownMatchSelectWidth={false}
                  onChange={debounce(changeFilterForm(form), 50)}
                >
                  {options}
                </Select>
              )}
            </Form.Item>
          );

          break;

        case 'Ext.Builder.ListFilterDate':
          const dateFormat = 'YYYY/MM/DD';

          const begin = filter.fieldValue[0] ? moment(filter.fieldValue[0], dateFormat) : '';
          const end = filter.fieldValue[1] ? moment(filter.fieldValue[1], dateFormat) : '';

          field = (
            <Form.Item>
              {getFieldDecorator(filter.fieldName, {
                initialValue: [begin, end],
              })(<DatePicker.RangePicker />)}
            </Form.Item>
          );
          break;

        case 'Ext.Builder.ListFilterButton':
          field = (
            <Form.Item>
              <Button
                onClick={this.handleFilterButton(filter.addParams)}
              >
                {filter.text}
              </Button>
            </Form.Item>
          );
          break;

        default:
          break;
      }

      return field;
    });

    return (
      <Form
        className={classNames('sk-cat-filter', { 'sk-cat-filter_sb-open': collapse })}
        layout="inline"
      >
        {filterFields.map(value => value)}
      </Form>
    );
  }
}

export default Index;
