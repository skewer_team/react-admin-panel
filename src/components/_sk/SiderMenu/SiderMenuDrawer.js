import React from 'react';
import { Drawer } from 'antd';
import SiderMenu from './Index';

const SiderMenuWrapper = React.memo(props => {
  const { isMobile, collapsed, onCollapse } = props;
  return isMobile ? (
    <Drawer
      visible={!collapsed}
      placement="left"
      onClose={() => onCollapse(true)}
      style={{
        padding: 0,
        height: '100vh',
      }}
    >
      <SiderMenu {...props} collapsed={collapsed} />
    </Drawer>
  ) : (
    <SiderMenu {...props} />
  );
});

export default SiderMenuWrapper;
