import React, { PureComponent, Suspense } from 'react';
import { Layout } from 'antd';
import classNames from 'classnames';
import Link from 'umi/link';
import {Route} from 'react-router-dom'
import styles from './index.less';
import PageLoading from '../../PageLoading';
import { title } from '../../../defaultSettings';
import LeftPanel from '../LeftPanel/Index';
import CanapeLogo from '../CanapeLogo/Index'
import RouterAdapter from '../RouterAdapter/Index'

const { Sider } = Layout;

let firstMount = true;

export default class SiderMenu extends PureComponent {
  componentDidMount() {
    firstMount = false;
  }

  render() {
    const { collapsed, onCollapse, fixSiderbar, theme, isMobile } = this.props;

    const siderClassName = classNames(styles.sider, {
      [styles.fixSiderBar]: fixSiderbar,
      [styles.light]: theme === 'light',
    });
    return (
      <Sider
        trigger={null}
        collapsible
        collapsed={collapsed}
        breakpoint="lg"
        onCollapse={collapse => {
          if (firstMount || !isMobile) {
            onCollapse(collapse);
          }
        }}
        width={310}
        theme={theme}
        className={siderClassName}
      >
        <div className={styles.logo} id="logo">
          <Link to="/">
            <CanapeLogo inverse />
          </Link>
        </div>
        <Suspense fallback={<PageLoading />}>

          <Route
            // path="/out.left.:leftPanelItemName=:leftPanelItemId"
            path="/:sectionName?/:tabName?"
            // eslint-disable-next-line react/no-children-prop
            children={(props) => {

              if (props.match) {

                const {sectionName, tabName} = props.match.params;

                return (
                  <RouterAdapter sectionName={sectionName} tabName={tabName}>
                    <LeftPanel collapsed={collapsed} />
                  </RouterAdapter>
                );


              } 
              return <LeftPanel />
            }}
          />

        </Suspense>
      </Sider>
    );
  }
}
