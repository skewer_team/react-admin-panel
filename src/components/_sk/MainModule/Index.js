import React from 'react';
import {connect} from "dva";
import AuthForm from "../AuthForm/Index";
import Layout from '../Layout/Index';

export default
connect(({ skGlobal }) => {
  return {
    moduleData: skGlobal.outLayout.get('out'),
  };
})(
  (props) => {

    const {
      moduleData,
      ...properties
    } = props;

    if (!moduleData)
      return null;

    let mainModule = null;

    if ( moduleData.get('moduleName') === 'Auth' ){
      mainModule =  <AuthForm {...properties} />;
    } else if ( moduleData.get('moduleName') === 'Layout' ){
      mainModule =  <Layout {...properties} />;
    }

    return mainModule;
  }
)
