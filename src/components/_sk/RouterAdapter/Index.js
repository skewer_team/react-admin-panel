import React, {Component} from 'react';
import { connect } from "dva";
import * as routerHelper from '../../../utils/_sk/router'

@connect()
class Index extends Component {

  componentDidMount() {

    const {dispatch, sectionName, tabName} = this.props;

    const {
      tabsItemName,
      leftPanelItemName,
      leftPanelItemId
    } = routerHelper.parseRouterParams(sectionName, tabName);

    // Сохраняем параметры роутинга
    dispatch({
      type: 'skGlobal/saveRouterParams',
      payload: {
        leftPanelItemName,
        leftPanelItemId,
        tabsItemName
      }
    });

  }

  componentDidUpdate(prevProps, prevState, snapshot) {

    const {dispatch, sectionName, tabName} = this.props;

    const {
      sectionName: sectionNamePrev,
      tabName: tabNamePrev
    } = prevProps;

    if ( (sectionNamePrev !== sectionName) || (tabNamePrev !== tabName) ){

      const {
        leftPanelItemName,
        leftPanelItemId,
        tabsItemName
      } = routerHelper.parseRouterParams(sectionName, tabName);


      dispatch({
        type: 'skGlobal/saveRouterParams',
        payload: {
          leftPanelItemName,
          leftPanelItemId,
          tabsItemName
        }
      });
    }

  }

  render() {

    const {children} = this.props;

    return children;
  }
}

export default Index;
