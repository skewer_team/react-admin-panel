import React from 'react';
import {Col, Divider, Row} from 'antd';
import Immutable from "immutable";
import styles from './style.less';
import DockedItem from "../DockedItem/Index";

const buildButtons = (buttonsData, handleClickOnButton) => {

  if (!buttonsData){
    return null;
  }

  return buttonsData
    .valueSeq()
    .map(value => {
      if (!Immutable.Map.isMap(value)) {
        return <Divider key={`divider_${_.uniqueId()}`} />;
      }

      const key = `${value.get('action')}_${value.get('state')}_${value.get('text')}`;

      return (
        <DockedItem
          key={key}
          configButton={value.toJS()}
          handleClickOnButton={handleClickOnButton(value.toJS())}
        />
      );
    });

};

export default ({ title, content, buttonsData, handleClickOnButton }) => {

  const titleHtml = title ? (
    <Row>
      <Col className="panel__header" span={24}>
        {title}
      </Col>
    </Row>
  ) : (
    null
  );

  return (
    <Row className="panel">
      {titleHtml}
      <Row type="flex">
        {
          buttonsData ? (
            <React.Fragment>
              <Col className="panel__leftColumn">
                {buildButtons(buttonsData, handleClickOnButton)}
              </Col>
              <Col className="panel__centerColumn">
                {content}
              </Col>
            </React.Fragment>
          ) : (
            <Col className="panel__centerColumn panel__centerColumn--alone">
              {content}
            </Col>
          )
        }

      </Row>
    </Row>
  );

}
