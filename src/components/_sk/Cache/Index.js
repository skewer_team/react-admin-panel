import React from 'react';
import {Button} from "antd";
import styles from './style.less';
import UilSync from '@iconscout/react-unicons/icons/uil-sync';

const handleClick = (props) => () => {

  const {dispatch} = props;

  dispatch({
    type: 'skGlobal/dropCache',
    payload: {
      path: 'out.header.cache'
    }
  });

};

export default (props) => {
  return (
    <Button
      className="top-nav_cache"
      // icon="reload"
      onClick={handleClick(props)}
    >
      <UilSync size="20" />
    </Button>
  );
}
