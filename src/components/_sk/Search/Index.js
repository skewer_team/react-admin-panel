import React from 'react';
import {AutoComplete, Input} from "antd";
import UilSearch from '@iconscout/react-unicons/icons/uil-search';
import styles from "./style.less";


const handleOnSelect = (value) => {

  // todo Хак
  const newUrl = String.prototype.replace.apply(value, [';', '/']);
  window.location.replace(newUrl);

};

const handleOnSearch = (props) => (value) => {

  const {dispatch} = props;

  dispatch({
    type: 'skGlobal/search',
    payload: {
      path: 'out.header.search',
      query: value
    }
  });

};

export default (props) => {

  const {moduleData} = props;

  if (!moduleData)
    return null;

  const langValues = moduleData.getIn(['init', 'lang']);

  const items = moduleData.getIn(['params', 'items']) || [];

  const dataSource = items.map(val => {
    return {
      value: val.get('url'),
      text: val.get('title')
    }
  });

  return (
    <AutoComplete
      className="nav-top_search"
      dataSource={dataSource}
      // style={{ marginRight: '10px', marginLeft: '22px', width: 600 }}
      onSelect={handleOnSelect}
      onSearch={handleOnSearch(props)}
      placeholder={langValues.get('searchSubText')}
    >
      <Input
        suffix={
          <UilSearch size="20" className="" />
        }
      />
    </AutoComplete>
  );
}
