import React from 'react';
import { connect } from 'dva';
import SkTree from '../skTree/Index';

const getOpenNodes = (moduleData) => {

  const parents = moduleData && moduleData.getIn(['params', 'parents'])
    ? moduleData.getIn(['params', 'parents']).toJS()
    : [];

  return parents.map(val => val.toString());
};

export default connect(({ skGlobal }) => {
  return {
    moduleData: skGlobal.leftLayout.get('out.left.lib'),
  };
})( ({ dispatch, moduleData }) => {

  const leftPanelItemId = moduleData.get('lastActiveItemId') || 0;

  return (
    <SkTree
      moduleName="lib"
      moduleData={moduleData}
      leftPanelItemId={leftPanelItemId}
      openNodes={getOpenNodes(moduleData)}
      dispatch={dispatch}
      typeNewPage={1}
    />
  );
})
