import React from 'react';
import { Button } from 'antd';
import Icon from '../Icon/Index'

const handleClickInForm = props => () => {

  const { handleClickOnButton, configButton } = props;
  handleClickOnButton(configButton);

};

export default (props) => {

  const { configButton } = props;

  if ( !configButton )
    return null;

  const { text } = configButton;

  return (
    <Button
      className="sk-left-col-button sk-main-button"
      type="primary"
      disabled={!!configButton.disabled}
      htmlType="button"
      block
      onClick={handleClickInForm(props)}
    >
      {
        configButton.iconCls
          ? (
            <span className="sk-left-btn__icon">
              <Icon alias={configButton.iconCls} />
            </span>
          ) : null
      }
      {text}
    </Button>
  );

}
