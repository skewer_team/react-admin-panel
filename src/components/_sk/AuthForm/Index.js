import React from 'react';
import { connect } from 'dva'
import style from './style.less'
import ForgotPassForm from "./ForgotPassForm";
import SuccessForm from "./SuccessForm";
import AuthForm from "./AuthForm";
import * as sk from "../../../services/_sk/api";


@connect(({ skGlobal }) => {
  return {
    moduleData: skGlobal.outLayout.get('out'),
  };
})
class Index extends React.Component {

  componentDidUpdate(prevProps, prevState, snapshot) {

    const {moduleData} = this.props;

    const params = moduleData.get('params');

    if ( params.get('cmd') === 'login' ){
      // если авторизация не удалась
      if ( !params.get('success') ) {
        // sk.error( params.notice );
      } else {
        window.location.reload();
      }
    }

  }


  render() {

    const {moduleData} = this.props;

    let renderComponent = null;

    switch (moduleData.getIn(['params', 'cmd'])){
      case 'checkForgot':
        const loginError = moduleData.getIn(['params', 'login']);
        if (loginError)
          sk.error(loginError);

        const captchaError = moduleData.getIn(['params', 'captcha']);
        if (captchaError)
          sk.error(captchaError);

        renderComponent = <ForgotPassForm />;
        break;

      case 'ForgotPass':
        renderComponent = <ForgotPassForm />;
        break;

      case 'Success':
        renderComponent = <SuccessForm />;
        break;

      case 'login':
        if (!moduleData.getIn(['params', 'success']))
          sk.error(moduleData.getIn(['params', 'notice']));
        renderComponent = <AuthForm />;
        break;

      default:
        renderComponent = <AuthForm />
    }

    return renderComponent;

  }
}


export default Index
