import React from 'react';
import { Collapse } from 'antd';
import router from 'umi/router'
import { connect } from "dva";
import TreeSection from '../TreeSection/Index';
import TreeTemplates from '../TreeTemplates/Index';
import TreeLibs from '../TreeLibs/Index';
import CatalogList from '../CatalogList/Index';
import ToolPanel from '../ToolPanel/Index';
import style from './style.less';
import { vh, addEvent } from '../helpers/Index'
import * as UilIcons from '@iconscout/react-unicons/';


@connect(({ skGlobal }) => {
  return {
    leftLayout: skGlobal.leftLayout,
    sidebarActiveItem: skGlobal.sidebarActiveItem
  }
})
class Index extends React.Component {
  constructor(props) {
    super(props);
    this.panelContent = React.createRef();
    this.state = {
      minHeight: 0,
      maxHeight: 0
    }
  }

  componentDidMount() {

    const { dispatch, leftPanelItemName, leftPanelItemId, tabsItemName } = this.props;
    // после маунта поиск высоты сайдбара
    dispatch({
      type: 'skGlobal/saveRouterParams',
      payload: {
        leftPanelItemName,
        leftPanelItemId,
        tabsItemName
      }
    });

    addEvent(window, "resize", (event)  => {
      this.calculateHeight();
    });

    this.calculateHeight();
  }



  componentDidUpdate(prevProps, prevState, snapshot) {

    const { dispatch, leftPanelItemName, leftPanelItemId, tabsItemName } = this.props;

    if ((prevProps.leftPanelItemName !== leftPanelItemName) ||
      (prevProps.leftPanelItemId !== leftPanelItemId) ||
      (prevProps.tabsItemName !== tabsItemName)) {

      dispatch({
        type: 'skGlobal/saveRouterParams',
        payload: {
          leftPanelItemName,
          leftPanelItemId,
          tabsItemName
        }
      });

    }
    // console.log(this.panelContent);
    // this.calculateHeight();
  }




  buildPanels = () => {

    const { leftLayout } = this.props;

    const { minHeight, maxHeight } = this.state 

    const { Panel } = Collapse;

    const panels = [];

    Object.entries(leftLayout.toJS()).forEach(([path, value]) => {

      const keyPanel = path.substring(path.lastIndexOf('.') + 1);

      let content = null;
      let icon = null;

      switch (keyPanel) {
        case 'section':
          content = <TreeSection />;
          icon = <UilIcons.UilListUl size="24" />
          break;
        case 'tpl':
          content = <TreeTemplates />;
          icon = <UilIcons.UilWindowGrid size="24" />
          break;
        case 'lib':
          content = <TreeLibs />;
          icon = <UilIcons.UilBookOpen size="24" />
          break;
        case 'catalog':
          content = <CatalogList />;
          icon = <UilIcons.UilApps size="24" />
          break;
        case 'tools':
          content = <ToolPanel />;
          icon = <UilIcons.UilSlidersVAlt size="24" />
          break;
        default:
      }
      const header = () => {
        return (
          <React.Fragment>
            <span className="sk-left-panel__header-icon">{icon}</span>
            <span title={leftLayout.getIn([path, 'init', 'title'])} className="sk-header-content">{leftLayout.getIn([path, 'init', 'title'])}</span>
          </React.Fragment>
        )
      }

      if (content) {
        panels.push(
          <Panel
            className={`sidebar-header sidebar-header-${keyPanel}`}
            header={header()}
            key={keyPanel}
          >
            <div className="sk-panel-content" style={{ minHeight, maxHeight }} ref={this.panelContent}>
              {content}
            </div>
          </Panel>
        );
      }

    });

    return panels;

  };

  calculateHeight() {
    if (this.panelContent.current.style) {
      const viewHeight = vh();
      const itemsCount = document.getElementsByClassName('sidebar-header').length
      const sbHeight = viewHeight - 75 - itemsCount * 44 - 73;
      this.setState({
        maxHeight: sbHeight,
        minHeight: sbHeight
      })
      // this.panelContent.current.style.minHeight = `${sbHeight}px`
      // this.panelContent.current.style.maxHeight = `${sbHeight}px`
    }
  }


  render() {

    const { sidebarActiveItem, leftLayout, collapsed } = this.props;

    const activeKey = (!collapsed) ? sidebarActiveItem : null;

    return (
      <Collapse
        className="sk-left-panel"
        accordion
        expandIconPosition="right"
        activeKey={activeKey}
        onChange={(newKey) => {

          if (newKey) {
            const lastActiveItemId = leftLayout.getIn([
              `out.left.${newKey}`, 'lastActiveItemId'
            ], 0);
            router.push({
              pathname: `/out.left.${newKey}=${lastActiveItemId}`
            });
          }

        }}
      >
        {this.buildPanels()}
      </Collapse>
    );
  }


}

export default Index
