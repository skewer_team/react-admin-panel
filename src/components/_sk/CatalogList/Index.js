import React from 'react';
import { connect } from 'dva';
import router from "umi/router";
import SkList from '../skList/Index';

const handleClickListItem = itemId => () => {

  router.push({
    pathname: `out.left.catalog=${itemId}`
  });

};

export default connect(({ skGlobal }) => {
  return {
    moduleData: skGlobal.leftLayout.get('out.left.catalog'),
  };
})(({ moduleData }) => {

  const leftPanelItemId = moduleData.get('lastActiveItemId') || 0;

  const dataSource = moduleData.getIn(['params', 'items']).toJS();

  return (
    <SkList
      items={dataSource}
      selectedItemId={leftPanelItemId}
      handleClick={handleClickListItem}
    />
  );
})
