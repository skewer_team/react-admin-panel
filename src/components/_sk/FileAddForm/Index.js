import React from 'react';
import {connect} from "dva";
import {Button, Form, Icon, Upload} from "antd";
import TestPanel from "../TestPanel/Index";


const normFile = e => {

  if (Array.isArray(e)) {
    return e;
  }
  return e && e.fileList;
};

const handleClickOnButton = (props) => (configButton) => () => {

  const {moduleData, dispatch, form} = props;

  const {state, action} = configButton;

  switch (state) {
    case 'upload':
      form.validateFields((err, values) => {
        if (!err) {
          console.log('Received values of form: ', values);
        }
      });
      break;

    default:

      dispatch({
        type: 'skGlobal/handleClickOnButton',
        payload: {
          path: moduleData.get('path'),
          action,
          configButton
        },
      });

  }

};



const buildContent = (props) => {

  const {moduleData, form, dispatch} = props;

  const {getFieldDecorator} = form;

  const serviceData = moduleData.getIn(['params', 'serviceData']).toJS();

  const uploadFieldConfig = {
    name: 'file0[]',
    action: '/admin/index.php',
    data: {
      ...serviceData,
      cmd: 'upload',
      sessionId: window.sessionId,
      path: moduleData.get('path'),
    },
    onChange(info) {
      if (info.file.status === 'done') {

        if (info.file.response.success || info.file.status === 'error') {

          dispatch({
            type: 'skGlobal/updateListFiles',
            payload: info.file.response.data
          });

        }

      }

    },
  };

  return (

    <Form
      {...{
        hideRequiredMark: true,
        labelAlign: 'left',
        colon: false
      }}
    >
      <Form.Item
        label={moduleData.getIn(["init", 'lang', 'fileBrowserFile'])}
      >
        {
          getFieldDecorator('file', {
            valuePropName: "fileList",
            getValueFromEvent: normFile,
            initialValue: [],
            rules: [{ required: true, message: '' }],
          })(

            <Upload {...uploadFieldConfig}>
              <Button>
                <Icon type="upload" /> Загрузить
              </Button>
            </Upload>
          )
        }
      </Form.Item>

    </Form>
  );

};

export default
connect(({ skGlobal }, { tabKey }) => {
  return {
    moduleData: skGlobal.tabs.get('out.tabs.lib_files'),
  };
})(
  Form.create({name: 'test_form'})(
    (props) => {

      const {moduleData} = props;

      return (
        <TestPanel
          title={moduleData.getIn(['params', 'panelTitle'])}
          content={buildContent(props)}
          buttonsData={moduleData.getIn(['params', 'dockedItems', 'left'])}
          handleClickOnButton={handleClickOnButton(props)}
        />
      );
    }
  )
)
