import React, { Component } from 'react'
import { Icon } from 'antd';
import UilBackward from '@iconscout/react-unicons/icons/uil-backward';
import UilForward from '@iconscout/react-unicons/icons/uil-forward';
import styles from './style.less';


export default class SiderCollapseButton extends React.Component {
  constructor(props) {
    super(props);
    
    this.content = <Icon type="menu-fold" style={{ fontSize: "24px" }}  />
    this.handleClick = this.handleClick.bind(this);
  }
  
  handleClick() {
    const { collapsed } = this.props
    
    const icon = (isCollapsed) => {
      // если свернуто меню
      if (isCollapsed) {
        // console.log("свернуто");
        // return <UilBackward size="24" />
        return <Icon type="menu-fold" style={{ fontSize: "24px" }} />

      }
      // развернуто меню
      // console.log("развернуто");
      // return <UilForward size="24" />
      return <Icon type="menu-unfold" style={{ fontSize: "24px" }}  />
    }
    
    this.content = icon(collapsed); 
  }
  

  render() {
    const { onClick } = this.props;
    return (
      <div
        onClick={() => { onClick(); this.handleClick(); }} 
        className="sk-menu-collapser"
      >
        {this.content}
      </div>
    )
  }



}


