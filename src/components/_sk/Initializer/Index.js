import React from 'react';
import { connect } from 'dva';

@connect()
class Index extends React.Component {

  componentDidMount() {

    const { dispatch } = this.props;

    // Запрос первичных данных
    dispatch({
      type: 'skGlobal/fetchInitData',
      payload: {},
    });

  }

  render() {
    return '';
  }

}

export default Index;
