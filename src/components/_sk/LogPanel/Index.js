import React, { Component } from 'react';
import classNames from 'classnames';
import {connect} from "dva";
import { Icon } from 'antd';
import styles from './Index.less';


@connect(({skGlobal}) => {
  return {
    errors: skGlobal.errors,
    moduleData: skGlobal.logLayout.get('out.log')
  };
})
class Index extends Component {
  state = {
    collapsed: true,
  };

  handleClick = () => {
    const { collapsed } = this.state;

    this.setState({
      collapsed: !collapsed,
    });
  };

  renderToolBar = () => {
    const { collapsed } = this.state;

    const {dispatch} = this.props;

    return !collapsed ? (
      <div className="logs-panel__toolbar">
        <Icon type="arrow-down" />
        <Icon
          type="delete"
          onClick={e => {
            e.stopPropagation();

            dispatch({
              type: 'skGlobal/clearErrors',
              payload: {}
            });

          }}
        />
      </div>
    ) : null;
  };

  render() {

    const {errors, moduleData} = this.props;

    const langLabel = moduleData.getIn(['init', 'lang']).toJS();

    const {collapsed} = this.state;

    return (
      <div className={classNames('logs-panel', { 'logs-panel--collapsed': collapsed })}>
        <div className="logs-panel__title" onClick={this.handleClick}>
          {langLabel.logPanelHeader}
          {this.renderToolBar()}
        </div>
        <div className="logs-panel__content">
          <ul>
            {errors.map((val, index) => {
              const keyItem = `key_log_panel_${index}`;
              return <li key={keyItem}><span dangerouslySetInnerHTML={{ __html: val }} /></li>
            })}
          </ul>
        </div>
      </div>
    );
  }
}

export default Index;
