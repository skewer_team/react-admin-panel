import React from 'react';
import {Helmet} from "react-helmet";
import { Skeleton, ConfigProvider } from 'antd';
import { connect } from 'dva';
import ruRU from 'antd/es/locale/ru_RU';
import enUS from 'antd/es/locale/en_US'
import deDE from 'antd/es/locale/de_DE'
import TabsDecorator from '../../../components/_sk/TabsDecorator/Index';
import * as routerHelper from '../../../utils/_sk/router'
import styles from './style.less';

const getLocaleByLang = (lang) => {
  let locale;

  switch (lang){
    case 'ru':
      locale = ruRU;
      break;
    case 'en':
      locale = enUS;
      break;
    case 'de':
      locale = deDE;
      break;
    default:
      throw new Error('Unsupported language');
  }

  return locale;

};

@connect(({ skGlobal, loading }) => {
  return {
    activeTab: skGlobal.activeTab,
    loading: loading.effects['skGlobal/fetchInitData'],
  };
})
class Index extends React.Component {

  render() {
    const { loading, activeTab, match } = this.props;

    const {sectionName, tabName} = match.params;

    const {
      tabsItemName,
      leftPanelItemName,
      leftPanelItemId
    } = routerHelper.parseRouterParams(sectionName, tabName);

    const currentLang = window.lang || 'ru';

    const locale = getLocaleByLang(currentLang);

    return (
      <ConfigProvider locale={locale}>
        <Helmet>
          <title>{window.titlePage || 'DevAdmin'}</title>
        </Helmet>
        <div className="b-main-container">
          <Skeleton active loading={loading}>
            {!match.params ? (
              <div style={{ padding: '10px 20px' }}>
                <h2>Canape CMS 4</h2>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aliquid culpa ea
                eum illo placeat sint sit vel. Ab autem blanditiis exercitationem incidunt nostrum
                sapiente tempore? Dicta ipsa provident quae.
              </div>
            ) : (
              <TabsDecorator
                leftPanelItemName={leftPanelItemName}
                leftPanelItemId={leftPanelItemId}
                tabsItemName={activeTab}
              />
            )}
          </Skeleton>
        </div>
      </ConfigProvider>
    );
  }
}

export default Index;
