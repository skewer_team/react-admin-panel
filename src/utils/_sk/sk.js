/**
 * Устанавливает значение в поле формы по заданному пути
 * @param path путь до файла (example: out.left.section)
 * @param fieldName имя поля для запрлнения
 * @param fieldValue новое значение
 */
export function setField(path, fieldName, fieldValue) {

  window.g_app._store.dispatch({
    type: 'skGlobal/updateForm2Param',
    payload: {
      path,
      fieldName,
      fieldValue,
    }
  });

}
